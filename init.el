;;; init.el --- `PeGnuMax' (Pegz' configuration of GNU Emacs) --- (@> "Code") -*- lexical-binding: t; -*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  ____       ____             __  __
;; |  _ \ ___ / ___|_ __  _   _|  \/  | __ ___  __
;; | |_) / _ \ |  _| '_ \| | | | |\/| |/ _` \ \/ /
;; |  __/  __/ |_| | | | | |_| | |  | | (_| |>  <
;; |_|   \___|\____|_| |_|\__,_|_|  |_|\__,_/_/\_\
;;
;; Copyright (C) 2020 Michael Pagan
;;
;; Author: (@url :file-name "mailto:Michael Pagan (pegzmasta) <michael.pagan@member.fsf.org>" :display "Michael Pagan (pegzmasta)")
;; Fingerprint: (@url :file-name "https://savannah.gnu.org/people/viewgpg.php?user_id=101089" :display "A99A 235B 599C 0894 20AF  2937 D675 7575 27CD F56D")
;; URL: https://gitlab.com/pegzmasta/dot-emacs/-/blob/master/init.el
;; Version: 1.0.0
;; Keywords: byte-compile debug .emacs.d freenet i2p startup tor use-package w3m wanderlust
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This file is NOT part of GNU Emacs. (@L "(about-emacs)")
;;
;; This configuration is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This configuration is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this configuration. If not, see (@url :file-name "http://www.gnu.org/licenses/" :display "Licenses").
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; (@* "Index")
;;
;;  If you have library `linkd.el', load `linkd.el' and turn on
;;  `linkd-mode' now.  It lets you easily navigate around the sections
;;  of this doc.  Linkd mode will highlight this Index, as well as the
;;  cross-references and section headings throughout this file.  You
;;  can get `linkd.el' here:
;;  https://www.emacswiki.org/emacs/download/linkd.el.
;;
;; (@> "Things Defined Here")
;; (@> "Commentary")
;; (@> "TL;DR")
;; (@> "Debug Init")
;; (@> "Autoloads")
;; (@> "Miscellaneous Functions")
;;   (@> "User Interface (UI)")
;;     (@> "Color Theme")
;; (@> "Early Startup Settings")
;;   (@> "Function Advisory")
;;   (@> "Hooks")
;;   (@> "Shell")
;; (@> "Pre-Package Management Settings")
;;   (@> "Load Path")
;;   (@> "Package Authentication")
;;   (@> "Package Manager")
;;   (@> "Ensure Packages")
;;     (@> "Environment")
;;   (@> "Package Updater")
;;   (@> "System Package Manager")
;;   (@> "Network Manager")
;;   (@> "GNU Downloader")
;;     (@> "Download Directory REGEX")
;;   (@> "Fonts")
;;   (@> "Dired Icons")
;; (@> "Clients For Different Network Protocols")
;;   (@> "Multimedia")
;;   (@> "eBook Readers")
;;   (@> "Feed Readers")
;;   (@> "Web Browsers")
;;     (@> "Lynx")
;;     (@> "Gopher")
;;       (@> "Gemini")
;;     (@> "Emacs Web Wowser")
;;     (@> "WWW Miru (Pager)")
;;       (@> "View Online Videos")
;;       (@> "Download Videos")
;;       (@> "Freenet")
;;       (@> "I2P")
;;       (@> "TOR")
;;       (@> "Proxy")
;;     (@> "Search Engines")
;;   (@> "Email")
;;     (@> "MIME")
;;     (@> "BBDB")
;;     (@> "SMTP")
;;     (@> "IMAP")
;;     (@> "BIFF")
;;   (@> "XMPP")
;;     (@> "ELIM - Emacs Lisp Instant Messenger (Alpha)")
;;     (@> "Finch")
;;       (@> "OMEMO")
;; (@> "Post-Package Management Settings")
;; (@> "Ergonomic Navigation Commands")
;; (@> "Emacs Aesthetics (Advanced UI)")
;;   (@> "Modeline")
;;     (@> "Date")
;;     (@> "Weather")
;;     (@> "Battery")
;;     (@> "Mode Icons")
;;       (@> "Emojis")
;;   (@> "Completion & Narrowing Framework")
;;   (@> "Dashboard")
;;   (@> "Games")
;; (@> "Editor Customizations")
;;   (@> "Image Processing")
;;   (@> "Text Editing Modes")
;;     (@> "Org Agenda")
;;     (@> "Org Babel")
;;     (@> "Org Mime")
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; (@* "Things Defined Here")
;;
;;  Commands defined here:
;;
;;    `add-mode-line-dirtrack', `add-property-with-date-captured',
;;    `agenda-color-char', `blink-cursor-timer-function', `buffer-empty-p',
;;    `choose-browser', `ck-message-decrypt-pgp-nonmime', `clear-comint',
;;    `close-and-kill-next-pane', `compile-w3m-type-ahead',
;;    `dired-toggle-read-only-and-icons', `djcb-popup',
;;    `djcb-wl-draft-attachment-check', `djcb-wl-draft-subject-check',
;;    `geiser-repl-buffer-name', `grok-bash-flossmanual', `jabber-finch',
;;    `jabber-install-elim', `jabber-install-lurch', `linkd-install',
;;    `lua-outline-mode', `lynx', `make-org-ready',
;;    `mime-edit-insert-multiple-files', `mime-preview-extract-current-entity',
;;    `mouse-avoidance-banish-destination', `my/disable-yornp',
;;    `my-org-agenda-format-date-aligned', `my/return-t', `my-scratch-buffer',
;;    `open-dashboard', `package--save-selected-packages', `powerline-moe-load',
;;    `read-bash-flossmanual', `reicon-dired-mode', `remove-output-buffer',
;;    `suppress-messages', `w3m-add-polipo-proxy-arguments',
;;    `w3m-adjust-youtube-channel', `w3m-auto-kill-page-process',
;;    `w3m-build-info', `w3m-check-freenet', `w3m-check-i2p', `w3m-check-tor',
;;    `w3m-download-correctly', `w3m-download-video',
;;    `w3m-download-video-at-point', `w3m-freenet', `w3m-freenet-install',
;;    `w3m-freenet-quit', `w3m-goto-custom-magnet', `w3m-i2p',
;;    `w3m-i2p-install', `w3m-i2p-quit', `w3m-info', `w3m-kill-page-process',
;;    `w3m-polipo-tor-kill-process', `w3m-polipo-tor-start-process',
;;    `w3m-polipo-tor-update-conf', `w3m-remove-proxy-arguments',
;;    `w3m-start-polipo-tor-inquiry', `w3m-tor-new-circuit',
;;    `w3m-tor-new-identity', `w3m-video', `w3m-view-video',
;;    `wdired-abort-changes-restore-icons', `wdired-finish-edit-restore-icons',
;;    `ytel-watch'.
;;
;;  Faces defined here:
;;
;;    `audio', `default-directory', `elfeed-face-symbol', `elpher-margin-key',
;;    `elpher-search', `extra-whitespace-face', `org-document-title',
;;    `org-level-1', `org-level-2', `org-level-3', `org-level-4', `org-level-5',
;;    `org-level-6', `org-special-keyword', `rainbow-delimiters-unmatched-face',
;;    `webcomic'.
;;
;;  Options (user variables) defined here:
;;
;;    `all-the-icons-latest-hack-version', `best-gc-cons-threshold',
;;    `blink-cursor-colors', `blink-cursor-count',
;;    `celestial-mode-line-phase-representation-alist',
;;    `celestial-mode-line-sunrise-sunset-alist', `dashboard-dir',
;;    `dired-omit-files', `elisp-dir', `elpa-dir', `emojify-image-dir',
;;    `eof-block-branches', `eof-block-reg', `format-flowed-entity',
;;    `freenet-signature', `grep-string', `i2p-signature', `last-command-char',
;;    `mode-icons-dir', `my-extra-keywords', `my-term-shell', `my-term-shell',
;;    `org-mime-library', `pegnumax--file-name-handler-alist',
;;    `pegnumax-first-run', `pegnumax-loading-p', `pegnumax-script',
;;    `pegnumax-string', `sixel-provided', `w3m-dir', `w3m-FProxy',
;;    `w3m-Invisible', `w3m-page-process', `w3m-polipo-process',
;;    `w3m-polipo-cache-dir', `w3m-polipo-conf-file', `w3m-polipo-port',
;;    `w3m-Privoxy-port', `w3m-supported-url', `w3m-tor-cache-dir',
;;    `w3m-tor-conf-file', `w3m-tor-port', `w3m-tor-process',
;;    `w3m-user-agent-odd', `w3m-user-agent-tor', `w3m-video-args',
;;    `w3m-video-executable', `w3m-view-other-title', `wget-dir'.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; (@* "Commentary") - Too much? (@> "TL;DR")
;;
;; This is the main startup file which is modified to work with the latest stable versions
;; of GNU Emacs.
;;
;; `PeGnuMax' is a *fast*, sleek, and optimized configuration for GNU Emacs!  Everything's
;; kept inside this single file, because I believe that if I start moving code outside of
;; this file and into external elisp files, then this configuration will not be as
;; stable, minimal, or as readable as I prefer.  Yes, ease of management, ease of use, and
;; program literacy (compact, yet readable and commented code), as well as backwards
;; compatibility (to a degree that makes sense) are important in this configuration.
;; This `init.el' file is designed to please you, the byte compiler, and all the programs
;; these instructions are set to install for you (few warnings/errors during compilation).
;;
;; `PeGnuMax' always runs in debug mode (this is an explicit design decision).  This
;; forces the user to use emacs correctly, as well as ensuring that packages are being used
;; and modified correctly.  If you see an error: You, a package maintainer, or package
;; contributor have done something wrong - simple as that.  Correct the bugs, if any, or
;; stop doing things incorrectly.  The *Messages* buffer will inform you of any warnings,
;; errors, or other generic events occurring within Emacs.  (@> "Elisp Stepper") is provided.
;;
;; Installing and removing packages is simple.  Add the provided name for the package
;; within the `package-selected-packages' list below (manually edit this variable; do not
;; `customize' it); create a `use-package' statement for said package; and then,
;; `restart-emacs'.  To uninstall packages, do the opposite: Remove the package name from
;; the `*-selected-packages' list; delete the `use-package' statement for said package;
;; and then, `restart-emacs'.  `use-package' will update your packages once a day at
;; `auto-package-update-at-time', and if you modify `*-selected-packages' and `use-package'
;; statements before the time indicated `*-at-time', then your changes will take place
;; `*-at-time' without you having to `restart-emacs'. Customize only with the :custom tags.
;;
;; This configuration has been tested to work from versions 25.1 to 27.1 of GNU Emacs.
;; There are 0 `require' statements in this configuration, and everything is either lazy
;; loaded (~100 packages), via `use-package', or autoloaded (10 packages).  There is only
;; 1 `load' command (optional) in this file, which loads a string from a file if it
;; exists.  By replacing `require' and `load' statements with `use-package' and `autoload'
;; statements, `init.el' (after the 1st/2nd initialization) now loads in only 1.2/1.3
;; seconds (with / without byte-compilation, respectively) and only 0.8/0.9 seconds with
;; emacs daemon on my computer (according to `emacs-init-time').  FYI, ~emacs -Q~ (no
;; initializations) yields a startup time of 0.5 seconds on my laptop, meaning: `PeGnuMax'
;; only adds 0.3 seconds to the startup time. `esup' reports startup time is "0.043sec!"
;; One thing to note, is that all packages (currently 3) with :upgrade labels can only be
;; upgraded by changing the value from 'nil to 't, and then running `eval-last-sexp'.  Make
;; sure to revert the value back to 'nil; otherwise, 3 seconds will be added to the startup
;; time.  You'll go from a 1-sec to a 4-sec startup.
;;
;; To get the most accurate `emacs-init-time', it is best to kill all daemons and X
;; applications before launching a fresh instance of GNU Emacs, for these processes *may*
;; effect the speed of Emacs' initialization.
;;
;; `PeGnuMax' is designed to be byte compiled, but not just to increase speed (you'll only
;; gain about 0.4 seconds); but, rather, in order to ensure that there are no warnings or
;; errors created by *any* of the instructions found in this file and other elisp programs
;; that `PeGnuMax' installs on your system.  A *stable* configuration?  You just found it!
;;
;; If this is your first install of my configuration, then please delete (or move) the
;; ELPA directory in your `user-emacs-directory' (e.g. "~/.emacs.d/") before loading
;; this file into Emacs.  A local elpa in `user-emacs-directory' will be built based
;; on the variable `package-selected-packages' (get friendly with it) found below.  A
;; bash script will be created and placed in your `user-emacs-directory'; suggestion: Do
;; *not* delete this script (only runs if you execute it manually; could've made any file,
;; but thought it might as well be a useful one), since `PeGnuMax' utilizes it to
;; determine if this is your first install of this configuration.  That's only a
;; suggestion, so do what you want.  Happy hacking!
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; (@* "TL;DR") - 1) empty `~/.emacs.d' | 2) Place `init.el' inside | 3) run `emacs'
;;
;;---------------------------------------------------------------------------------------------------
;; 1 | (@L "(read-bash-flossmanual)")
;;---'
;;
;; # Prepare the user's emacs directory (from Bash, commands to execute are after the `$'):
;; $ mkdir -p ~/.emacs.d/
;; $ rm -rf ~/.emacs.d/elpa ~/.emacs.d/elisp
;;---------------------------------------------------------------------------------------------------
;; 2 | (@L "(tldr \"chmod\")")
;;---'
;;
;; # First time installation:
;; $ cd ~/.emacs.d/
;; $ git clone https://gitlab.com/pegzmasta/dot-emacs.git
;; $ mv dot-emacs/* .
;; $ rm -rf dot-emacs/
;; $ emacs # The below is _optional_
;; $ chmod +x ~/.emacs.d/pegnumax.sh # Try binding to a GNOME (or other) keyboard shortcut
;;---------------------------------------------------------------------------------------------------
;; 3 | (@info :file-name "emacs" :node "emacsclient Options" :display "emacsclient Options")
;;---'
;;
;; # Subsequent use (after first installation and use):
;; $ emacsclient -c -a '' -e '(powerline-moe-load)'
;; $ ~/.emacs.d/pegnumax.sh # The above is equivalent
;; # Make sure to simply invoke `emacs' after modifying `package-selected-packages'
;;---------------------------------------------------------------------------------------------------
;; 4 | (@L "(my-scratch-buffer)")
;;---'
;;
;; Once you execute `emacs' or `emacsclient' you will be sent to the *scratch* buffer, with
;; a guide inserted within that buffer.  Everything you need to use GNU Emacs is there.
;;
;; Happy Hacking!
;;
;;---------------------------------------------------------------------------------------------------
;; 5 |
;;---'
;;
;; Still reading?
;;
;; If you are making many modifications or are simply hacking like crazy on `PeGnuMax',
;; then do _not_ exit out of your current session of Emacs!
;;
;; Instead:
;;   1. Create a new directory outside of `user-emacs-directory'
;;   2. Add: `export EMACS_USER_DIRECTORY="<your-testing-directory>"` to your (@file :file-name "~/.bashrc" :display "~/.bashrc")
;;      See (@> "Environment")
;;   3. Start a new instance of Emacs.  No need for `--debug-init`, `PeGnuMax' is always in
;;      debug mode - period.
;;   4. If there are errors loading your modified configuration:
;;      $ cd <your-testing-directory>; rm -rf *; emacs # Hack, rinse, repeat
;;
;; The benefit here is that you will not have to leave your current Emacs session and be
;; forced to hack within a corrupt or error-prone environment.  When you are done making
;; changes to `PeGnuMax' and <your-testing-directory> was loaded without errors, then you
;; can remove <your-testing-directory>, and-- if you wish-- remove `elpa-dir', `elisp-dir',
;; and `pegnumax.sh' in `user-emacs-directory' and `restart-emacs'.  Your updates, without
;; errors, will take effect once Emacs loads again.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; (@> "Code")
;; (set the selective display with: ~C-u C-x $~ or ~C-u 2 C-x $~)
;; (@L "(set-selective-display 0)") -- Reset the display
;; (@L "(set-selective-display 4)")
;; (@L "(set-selective-display 8)")

;; (@* "Debug Init")
;; Debugging options; var `byte-compile-warnings' may also prove useful.
;; Hold down the chord ~C-M-f~ to discover any paren errors within sexp's, or use (@L "(check-parens)").
(setq debug-on-quit  nil
      debug-on-error t
      debugger-stack-frame-as-list t
      byte-compile-warnings '(cl-functions))
(push "\\`XML:.*" debug-ignored-errors)                        ;; some RSS feeds are malformed...
(push 'file-error debug-ignored-errors)                       ;; faulty gopherholes
(push 'bad-signature debug-ignored-errors)
(push "\\`No such page" debug-ignored-errors)
(push "\\`Frame is not zoomed" debug-ignored-errors)
(push "\\`Selecting deleted buffer" debug-ignored-errors)      ;; In case the Dashboard loses it's agenda (~C-a o~)
(push "\\`Cannot find minor mode for" debug-ignored-errors)    ;; Click on the window number with the mouse
(push "\\`.*Name or service not known" debug-ignored-errors)   ;; faulty gopherholes
(push "\\`Opened .* in external program" debug-ignored-errors) ;; hard-coded in `openwith'
(push "\\`.*No address associated with hostname" debug-ignored-errors)
(push "\\`Attempt to delete minibuffer or sole ordinary window" debug-ignored-errors)
(push "\\`Command attempted to use minibuffer while in minibuffer" debug-ignored-errors)
;; (@> "Elisp Stepper")
;; (@L "(fset 'elisp-stepper (kbd \"C-u C-l C-M-f C-x C-e\"))")
;; (@L "(global-set-key [f8] 'elisp-stepper)")
;; (@L "(fset 'messages-step (kbd \"C-x b init.el RET C-x 1 C-x 2 C-x o C-x b *Messages* RET C-x o\"))")
;; (@L "(global-set-key [f9] 'messages-step)")

;; (@* "Autoloads")
(eval-when-compile
  (autoload 'quelpa "quelpa" t)
  (autoload 'use-package "use-package-core" t)
  (autoload 'benchmark-init/deactivate "benchmark-init" t)
  (autoload 'volume "volume" "Tweak your sound card volume." t)
  (autoload 'ducpel "ducpel" nil t) ;; Logic game with sokoban elements
  (autoload 'sudoku "sudoku" "Start playing sudoku." t))

;; (@* "Miscellaneous Functions")
(eval-and-compile
  (autoload 'w3m "w3m" nil t)
  (defun buffer-empty-p (&optional buffer) (= (buffer-size buffer) 0))
  (defun close-and-kill-next-pane nil
    "If there are multiple windows, then close the other pane and kill the buffer in it also."
    (interactive) (other-window 1) (kill-this-buffer) (if (not (one-window-p)) (delete-window)))
  ;; Snippet source: (@url :file-name "https://superuser.com/questions/669701/emacs-disable-some-minibuffer-messages#1025827" :display "Emacs - Disable Some Minibuffer Messages")
  (defun suppress-messages (old-fun &rest args)
    (cl-flet ((silence (&rest _args1) (ignore)))
      (advice-add 'message :around #'silence)
      (unwind-protect
          (apply old-fun args) (ignore old-fun) (ignore args)
          (advice-remove 'message #'silence))))
  ;; Snippet source: (@url :file-name "https://stackoverflow.com/questions/6591043/how-to-answer-yes-or-no-automatically-in-emacs#35263420" :display "how to answer yes or no automatically in emacs")
  (defun my/return-t (_orig-fun &rest _args) t)
  (defun my/disable-yornp (orig-fun &rest args)
    (advice-add 'yes-or-no-p :around #'my/return-t)
    (advice-add 'y-or-n-p :around #'my/return-t)
    (let ((res (apply orig-fun args)))
      (advice-remove 'yes-or-no-p #'my/return-t)
      (advice-remove 'y-or-n-p #'my/return-t) res))

  ;; (@* "User Interface (UI)")
  (with-eval-after-load "which-key"
    (defun linkd-install nil
      (remove-hook 'wget-after-hook 'linkd-install)
      (shell-command (concat "cd " elisp-dir
                             "; mv ~/Code/linkd.tar.gz .; tar -zxvf linkd.tar.gz; "
                             "mv ../linkd.el .; rm linkd.tar.gz"))
      ;; Bugfix for (@file :file-name "~/.emacs.d/elisp/linkd.el" :to "Lisp Links" :display "Lisp Links")
      ;; Ensure proper evaluation of elisp code.
      ;; Ensure `linkd-command' is overlayed with the "command" icon - not the "url" icon.
      (shell-command (concat "cd " elisp-dir "; "
                             "sed -i 's/(message .... (eval ,sexp)))/(eval (car (read-from-string ,sexp))))/' linkd.el && "
                             "sed -i '/[^ ]linkd-command/ {n;s/url/command/}' linkd.el"))
      ;; Bugfix for string (@file :file-name "~/.emacs.d/elisp/linkd.el" :to "TODO" :display "(@* or @>)") in TODO.  `linkd' link face overlay error.
      ;; This error sends me straight to the debugger upon opening the file.
      (shell-command (concat "cd " elisp-dir "; "
                             "sed -i 's/just to the next link.\*/just to the next link @\* or @>./' linkd.el"))
      ;; Bugfix for (@file :file-name "~/.emacs.d/elisp/linkd.el" :to "Recognizing Links" :display "linkd-match")
      ;; Removing the extra parenthesis ensures proper compilation of `linkd.el'.
      (shell-command (concat "cd " elisp-dir "; "
                             "sed -i 's/.error nil./error nil/' linkd.el"))
      (if (file-exists-p (concat elisp-dir "linkd.elc"))
          (delete-file (concat elisp-dir "linkd.elc")))
      (byte-compile-file (concat elisp-dir "linkd.el")))
    (unless (eq pegnumax-first-run 't)
      (unless (file-exists-p (concat elisp-dir "linkd.el"))
        (wget "http://www.emacswiki.org/emacs/download/linkd.tar.gz")
        (add-hook 'wget-after-hook 'linkd-install)))
    ;; Inspired by (@> "GNU Eev"); consider adding (@url :file-name "https://github.com/nobiot/org-transclusion" :display "Org-transclusion").
    (use-package linkd
      ;; Learn more: (@url :file-name "https://web.archive.org/web/20120427055222/http://dto.github.com:80/notebook/linkd.html" :display "linkd (archived)"); Example Usages: (@url :file-name "https://web.archive.org/web/20070327141032/http://dto.freeshell.org/notebook/Dtox.html" :display "DTOX: DeskTop Odyssey eXtended")
      ;; Replacement: (@url :file-name "https://web.archive.org/web/20070704045310/http://dto.freeshell.org/notebook/Eon.html" :display "Eon"); (@url :file-name "https://web.archive.org/web/20080602211121/http://dto.mamalala.org/eon/" :display "Index of /eon")
      :ensure nil
      :load-path "elisp/"
      :preface
      (declare-function linkd-mode "linkd.el" (&optional ARG))
      :commands (linkd-mode)
      :after shell
      :custom
      (linkd-use-icons t)
      (linkd-icons-directory (concat elisp-dir "icons"))
      :config (setq linkd-process-block-function 'linkd-send-block-to-shell))
    (defun powerline-moe-load nil
      (interactive)
      (when (daemonp)
        (set-frame-parameter nil 'fullscreen 'fullboth)
        (unless (eq pegnumax-first-run 't)
          (add-hook 'prog-mode-hook 'linkd-mode)))
      ;; (@> "Fonts")
      (when (find-font (font-spec :name "Hack"))
        (set-face-attribute 'default nil :font "Hack" :height 90)
        (set-frame-font "Hack" nil t))

      ;; (@* "Color Theme")
      (unless (fboundp 'linkd-mode) ;; Do not reload themes after calling the daemon again.
        (use-package greenbar ;; This package makes it easier to read through a *shell* buffer for horizontally long output.
          :init
          (defun greenbar-color-themes-reset nil
            "Ensure that `greenbar-color-themes' matches `moe-theme-which-enabled'."
            (interactive)
            (setq greenbar-color-themes
                  (list
                   (cons 'greenbar
                         (if (eq (frame-parameter nil 'background-mode) 'dark)
                             '("#344034" "#343434")
                           '("#e4f0e4" "#f0f0f0")))
                   (cons 'graybar
                         (list
                          (if (eq (frame-parameter nil 'background-mode) 'dark)
                              "gray30" "gray70")
                          (face-background 'default)))
                   (cons 'rainbow
                         (let ((x (if (eq (frame-parameter nil 'background-mode) 'dark) "40" "f0"))
                               (o (if (eq (frame-parameter nil 'background-mode) 'dark) "34" "e4")))

                           (mapcar (lambda (c) (apply #'concat "#" c))
                                   `((,x ,x ,o) (,x ,o ,o) (,x ,o ,x) (,o ,o ,x) (,o ,x ,x) (,o ,x ,o)))))))))
        (use-package moe-theme
          :unless noninteractive
          :commands powerline-moe-theme
          :init
          (use-package powerline
            :demand t
            :commands (powerline-center-theme powerline-revert)
            :custom
            (powerline-default-separator 'arrow-fade) ;; Fading helps blend differing colors :-)
            (powerline-default-separator-dir '(right . left))) ;; Better suits `*-center-theme'
          (use-package moe-theme-switcher
            :ensure nil
            :config
            (setq moe-theme-mode-line-color 'green)
            (setq moe-theme-highlight-buffer-id nil)
            (setq moe-theme-colorize-modeline-by-frame-id t)
            (powerline-moe-theme)
            (set-face-attribute 'powerline-active2 nil :background "#585858" :foreground "#ffffff")
            (powerline-revert)
            (powerline-center-theme) ;; No EOL indication; however, `powerline-default-theme' has it
            (setq moe-theme-revert-theme nil) ;; Required for `moe-magenta-hack'
            (execute-kbd-macro (read-kbd-macro "M-x moe-theme-select-color RET magenta RET")) ;; `moe-magenta-hack'
            (setq moe-theme-revert-theme t))))))

  ;; (@> "Shell")
  (with-eval-after-load 'shell
    (defun add-mode-line-dirtrack ()
      (add-to-list 'mode-line-buffer-identification
                   '(:propertize (" " default-directory " ") face dired-directory)))
    (defun clear-comint nil "Clears the screen (like a proper shell), and resets the greenbar theme.  Bound to C-l."
           (interactive) (let ((comint-buffer-maximum-size 0)) (comint-truncate-buffer) (greenbar-color-themes-reset)))
    (defun compile-w3m-type-ahead nil
      (remove-hook 'wget-after-hook 'compile-w3m-type-ahead)
      (byte-compile-file (concat elisp-dir "w3m-type-ahead.el"))
      ;; Prevent error with meta-variable `hierarchy--make', used by (@> "Mode for Reddit")
      (defvar hierarchy-dir
        (substring (shell-command-to-string (concat "ls " elpa-dir " | grep 'hierarchy-[0-9]*\.[0-9]*$'")) 0 -1))
      (delete-file (concat elpa-dir hierarchy-dir "/hierarchy.elc"))
      ;; Compiling during the second run of `PeGnuMax' ensures package `hierarchy' can be used by (@> "Mode for Reddit")
      (byte-compile-file (concat elpa-dir hierarchy-dir "/hierarchy.el"))
      ;; If this is the second run of `PeGnuMax', then handle it accordingly.
      (if (package-installed-p 'restart-emacs)
          (if (yes-or-no-p "Welcome to `PeGnuMax'!  Restart (second time), so that changes to GNU Emacs can properly take effect? ")
              (restart-emacs))))
    (defun w3m-build-info nil
      "Build the `emacs-w3m' info file."
      (message "Building `emacs-w3m' INFO file...")
      (async-shell-command
       (concat "cd " user-emacs-directory "quelpa/build/w3m/; "
               "autoconf && ./configure && cd doc; make emacs-w3m.info"))
      (if (info-file-exists-p (concat user-emacs-directory "quelpa/build/w3m/doc/emacs-w3m.info"))
          (message "Success: Makefile has built file `emacs-w3m.info'!")
        (error "Error: Makefile failed to build file `emacs-w3m.info'!")))
    (defun w3m-info nil
      "View the `emacs-w3m' info file."
      (interactive)
      (defvar w3m-dir
        (substring (shell-command-to-string (concat "ls " elpa-dir " | grep 'w3m-[0-9]*\.[0-9]*$'")) 0 -1)
        "The version number is a part of the directory name, hence it's variable, thus this variable.")
      ;; Quelpa successfully built the info file from (@> "Boruch's Repo").
      (if (info-file-exists-p (concat elpa-dir w3m-dir "/emacs-w3m.info"))
          (Info-find-node "emacs-w3m" "Top")
        ;; Quelpa failed to build the info file from (@> "Boruch's Repo").
        (if (info-file-exists-p (concat user-emacs-directory "quelpa/build/w3m/doc/emacs-w3m.info"))
            (progn
              (global-highlight-changes-mode -1)
              (find-file (concat user-emacs-directory "quelpa/build/w3m/doc/emacs-w3m.info"))
              (Info-on-current-buffer)
              (global-highlight-changes-mode +1))
          (w3m-build-info))))))

;; (@* "Early Startup Settings")
(if (not (or (eq system-type 'gnu) (eq system-type 'gnu/linux) (eq system-type 'gnu/kfreebsd)))
    (error (concat "Detected system, %s - Point-blank: This specific configuration requires a *GNU* system to run GNU Emacs"
                   " - other systems are unsupported." system-type))
  (if (version< emacs-version "25.1")
      (error "Detected Emacs %s.  This configuration requires GNU Emacs 25.1 and above!" emacs-version)

    ;; Allow Emacs to run in a way that allows for other configurations to exist - see (@> "Environment").
    ;; (@url :file-name "https://gonewest818.github.io/2020/03/a-standalone-init.el-for-emacs-package-debugging/" :display "A standalone init.el for Emacs package debugging")
    (if (getenv "EMACS_USER_DIRECTORY") ;; Not included in the environment if Emacs is launched via GNOME Keyboard Shortcut.
        (if (file-directory-p (getenv "EMACS_USER_DIRECTORY"))
            (setq user-emacs-directory (getenv "EMACS_USER_DIRECTORY"))))

    ;; Reducing GC frequency, in order to ensure a fast startup time.
    ;; Starting (@> "Garbage Collection").
    (defvar best-gc-cons-threshold gc-cons-threshold "Default garbage collection threshold value.")
    (setq gc-cons-threshold most-positive-fixnum)

    ;; At startup, Emacs is not going to be loading any files; therefore, no file name handling.
    (defvar pegnumax--file-name-handler-alist file-name-handler-alist)
    (setq file-name-handler-alist nil)

    ;; One less file to load at startup.
    (setq site-run-file nil)

    ;; The existence of (@file :file-name "~/.emacs.d/pegnumax.sh" :display "pegnumax.sh") determines if we're running this configuration for the first time.
    (defvar pegnumax-loading-p t "If emacs is still loading, value is 't; otherwise, 'nil.")
    (defvar pegnumax-first-run nil "If this is the first run of `PeGnuMax', then startup will vary.")
    (defvar elpa-dir (concat user-emacs-directory "elpa/") "Location of our emacs lisp packages.")
    (defvar elisp-dir (concat user-emacs-directory "elisp/") "Location of our obscure or custom emacs lisp files.")
    (unless (file-exists-p (concat user-emacs-directory "pegnumax.sh"))
      (setq pegnumax-first-run t)
      (setq gc-cons-threshold best-gc-cons-threshold) ;; On startup during first use: Collect garbage _normally_.
      (if (file-directory-p elpa-dir)
          (error (concat "`elpa' or `elisp' directories are in \"" user-emacs-directory "\".  Remove them,
      `PeGnuMax' will create new versions of them."))))

    ;; Go fullscreen, eliminate some GUI elements, and enable some useful features.
    (set-frame-parameter nil 'fullscreen 'fullboth)
    (mapc (lambda (mode) (when (fboundp mode) (apply mode '(-1))))
          '(menu-bar-mode scroll-bar-mode tool-bar-mode))
    (put 'upcase-region   'disabled nil)
    (put 'downcase-region 'disabled nil)

    ;; If we're in GNU Emacs 26+, then use new Emacs features (while obsoleting old ones).
    (or (version< emacs-version "26.1") (version< emacs-version "26.2") (version< emacs-version "26.3")
       (fset 'linum-mode 'display-line-numbers-mode))
    (when (version< "27" emacs-version) (fset 'fci-mode 'display-fill-column-indicator-mode))

    ;; For you, and other programs ;-)
    (fset 'yes-or-no-p 'y-or-n-p)                        ;; For you
    (fset 'function* 'cl-function)                       ;; For `md4rd.el'
    (fset 'assoc-ignore-case 'assoc-string)              ;; For `jabber.el'
    (defvaralias 'last-command-char 'last-command-event) ;; For (@file :file-name "~/.emacs.d/elisp/w3m-type-ahead.el" :display "w3m-type-ahead.el")
    (defvar blink-cursor-count "" "In case you err out of startup too early.")
    (defvar blink-cursor-colors
      (list "#ffafd7" "#ffafaf" "#ffd787" "#fce94f" "#afdf77" "#87d7af" "#afd7ff" "#e6a8df")
      "On each blink the cursor will cycle to the next color in this list.
Copied from ':background' tag inside `moe-theme-mode-line-color'.")
    (defface extra-whitespace-face '((t (:background "pale green")))
      "Used for tabs and such." :group 'whitespace-faces)
    (defvar my-extra-keywords '(("\t" . 'extra-whitespace-face)))
    (defvar my-term-shell (substring (shell-command-to-string "which bash") 0 -1))

    ;; (@* "Function Advisory")
    (defadvice ansi-term (before force-bash)
      (interactive (list my-term-shell)))
    (ad-activate 'ansi-term)
    (defadvice shell (after kill-with-no-query nil activate)
      (set-process-query-on-exit-flag (get-buffer-process ad-return-value) nil))
    (advice-add 'sunrise-sunset :around #'suppress-messages)
    (advice-add 'package-install-selected-packages :around #'my/disable-yornp) ;; To avoid hidden prompts.
    (advice-add 'upcase-region
                :around
                (lambda(oldfun &rest args)
                  "Only apply upcase-region when region active."
                  (when (region-active-p)
                    (apply oldfun args))))
    (advice-add 'downcase-region
                :around
                (lambda(oldfun &rest args)
                  "Only apply downcase-region when region active."
                  (when (region-active-p)
                    (apply oldfun args))))
    ;; (@* "Hooks")
    (add-hook 'org-mode-hook 'make-org-ready) ;; (@> "Org Ready?") This has to be called twice for some reason (2nd Call).
    (add-hook 'window-setup-hook 'delete-other-windows)
    (add-hook 'c-mode-common-hook (lambda nil (c-set-style "gnu")))
    (add-hook 'write-file-functions 'highlight-changes-rotate-faces nil)
    (add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)
    (add-hook 'comint-mode-hook (lambda nil (local-set-key (kbd "C-l") 'clear-comint) (greenbar-mode)))
    ;; (@info :file-name "woman" :display "Browse UN*X Manual Pages \"W.O. (without) Man\".")
    (add-hook 'woman-mode-hook (lambda nil (if (get-buffer "*WoMan-Log*") (kill-buffer "*WoMan-Log*"))))
    (add-hook 'text-mode-hook (lambda nil (setq show-trailing-whitespace t) (font-lock-add-keywords nil my-extra-keywords)))
    (unless (eq pegnumax-first-run 't)
      (add-hook 'Custom-mode-hook 'turn-on-tempbuf-mode)
      (add-hook 'backtrace-mode-hook 'turn-on-tempbuf-mode)
      (add-hook 'compilation-mode-hook 'turn-on-tempbuf-mode)
      (add-hook 'wget-after-hook
                (lambda nil
                  (if (get-buffer "*wget*")
                      (kill-buffer "*wget*")))))
    (add-hook 'minibuffer-exit-hook (lambda nil
                                      (let ((buffer "*Completions*"))
                                        (and (get-buffer buffer)
                                           (kill-buffer buffer)))))
    (add-hook 'find-file-hook (lambda nil
                                (fci-mode)
                                (whitespace-mode)
                                (unless (eq pegnumax-first-run 't) (turn-on-tempbuf-mode))))
    (add-hook 'esup-mode-hook
              (lambda nil
                (if (get-buffer "*esup-log*")     (kill-buffer "*esup-log*"))
                (if (get-buffer "*esup-child*")   (kill-buffer "*esup-child*"))
                (if (get-buffer "*esup-results*") (kill-buffer "*esup-results*"))))
    (add-hook 'shell-mode-hook
              (lambda nil
                (add-mode-line-dirtrack)
                (greenbar-color-themes-reset)
                ;; (@> "PS1") - Allow a colorful shell prompt
                (face-remap-set-base 'comint-highlight-prompt :inherit nil)))
    (add-hook 'after-init-hook
              (lambda nil
                (setq-default visible-bell                          t
                              tab-width                             2
                              indent-tabs-mode                      nil
                              fill-column                         158
                              whitespace-line-column      fill-column
                              whitespace-style     '(face lines-tail)
                              woman-fill-column                   100
                              woman-default-indent                  7
                              indicate-empty-lines                  t
                              echo-keystrokes                     0.1
                              blink-cursor-count                    0
                              x-stretch-cursor                      t
                              initial-buffer-choice                 t
                              inhibit-startup-screen                t
                              inhibit-startup-buffer-menu           t
                              completion-ignore-case                t
                              show-paren-style            'expression
                              show-trailing-whitespace              nil
                              read-file-name-completion-ignore-case t
                              load-prefer-newer                     t)
                (defun remove-output-buffer nil
                  (if (get-buffer  "*ESS*")
                      (kill-buffer "*ESS*"))
                  (if (get-buffer  "*Error*")
                      (kill-buffer "*Error*"))
                  (if (get-buffer  "*Dired log*")
                      (kill-buffer "*Dired log*"))
                  (if (get-buffer  "*info*")
                      (kill-buffer "*info*"))
                  (if (get-buffer  "org-loaddefs.el")
                      (kill-buffer "org-loaddefs.el"))
                  (if (get-buffer  "*quelpa-build-checkout*")
                      (kill-buffer "*quelpa-build-checkout*"))
                  (if (get-buffer  "*quelpa-build-info*")
                      (kill-buffer "*quelpa-build-info*"))
                  (if (get-buffer  "*Shell Command Output*")
                      (kill-buffer "*Shell Command Output*"))
                  (if (get-buffer  "*Warnings*")
                      (kill-buffer "*Warnings*"))
                  (if (get-buffer  "*Compile-Log*")
                      (kill-buffer "*Compile-Log*")))
                (defun grok-bash-flossmanual nil
                  "Read a GNU bash (the Bourne Again Shell) FLOSS manual via `nov-mode'"
                  (remove-hook 'wget-after-hook 'grok-bash-flossmanual)
                  (find-file "~/Documents/command-line.epub")
                  (unless (fboundp 'nov-reopen-as-archive) (nov-mode)))
                (defun read-bash-flossmanual nil
                  "Download an ePub about the GNU command-line."
                  (interactive)
                  (if (file-exists-p "~/Documents/command-line.epub")
                      (grok-bash-flossmanual)
                    (progn
                      (wget "http://booki.flossmanuals.net/_booki/command-line/command-line.epub")
                      (add-hook 'wget-after-hook 'grok-bash-flossmanual)))) ;; Can't read it until after download.
                ;; Snippet source: (@url :file-name "https://stackoverflow.com/questions/4642835/how-to-change-the-cursor-color-on-emacs?answertab=active#tab-top" :display "how to change the cursor color on emacs")
                (defun blink-cursor-timer-function nil
                  "Zarza wrote this cyberpunk variant of timer `blink-cursor-timer'.
Warning: overwrites original version in `frame.el'.
This one changes the cursor color on each blink. Define colors in `blink-cursor-colors'."
                  (when (not (internal-show-cursor-p))
                    (when (>= blink-cursor-count (length blink-cursor-colors))
                      (setq blink-cursor-count 0))
                    (set-cursor-color (nth blink-cursor-count blink-cursor-colors))
                    (setq blink-cursor-count (+ 1 blink-cursor-count)))
                  (internal-show-cursor nil (not (internal-show-cursor-p))))
                (defun my-scratch-buffer nil
                  "Create a fancy *scratch* buffer for the advanced user.  Press <F12> to restore *scratch* if killed."
                  (interactive)
                  (setq
                   initial-scratch-message
                   (purecopy
                    (concat
                     "(\@L \"(open-dashboard)\")\n" ;; (@> "Dashboard")
                     ";; Greetings, " user-full-name ":\n"
                     ";; I am your editor.  Welcome to `PeGnuMax' (i.e. \"Peh-Guh-new-M[a,e,i]x\")!\n;;\n;;"
                     " This is a slightly modified vanilla version of *the*\n;; GNU Emacs-- the extensible,"
                     " customizable, self-documenting real-time display editor.\n;;\n;; This configuration"
                     " lives inside of \"" user-init-file "\", where even this very\n;; string"
                     " (`initial-scratch-message') can be modified.  Enter \"C-M-f\" (Ctl-Alt-f) to move to\n;;"
                     " the next symbolic expression (sexp), and then enter \"C-x C-e\" in order to evaluate the last"
                     " sexp\n;; (the one just before the blinking cursor); \"C-j\" to evaluate and print the last"
                     " sexp.  Feel free\n;; to play around this buffer (or venture off elsewhere-- your choice),"
                     " since nothing will be saved.\n\n;; Here's a tutorial\n(help-with-tutorial)\n\n;; Learn more"
                     " about GNU Emacs\n(about-emacs)\n\n;; Learn how to use the Info system, via the info reader\n"
                     "(Info-help)\n\n;; The GNU Emacs maintainers have answered many questions.\n;; If you have any,"
                     " then read through the FAQ.\n(info \"efaq\")\n\n;; Gone through the tutorial?  Learned about"
                     " Emacs?  Let's RTFM, I mean... uh, read the manual :-)\n(info-emacs-manual)\n\n;; Your home"
                     " directory, Emacs directory, and Desktop (respectively)\n(dired \"~/\")\n(dired \""
                     user-emacs-directory "\")\n(dired \"~/Desktop/\")\n\n;; This link provides tips for effective"
                     " emacs productivity.\n(w3m-browse-url \"http://ergoemacs.org/emacs/effective_emacs.html\")\n\n"
                     ";; Ready to go the extra mile and master/grok Emacs?  Check out the wiki online!\n"
                     "(w3m-browse-url \"https://www.emacswiki.org/\")\n\n;; Hack `PeGnuMax'\n(find-file \""
                     user-emacs-directory "init.el\")\n\n;; Grok GNU Bash... err, master the commandline with a "
                     "FLOSS manual in `nov-mode'.\n(read-bash-flossmanual)\n(shell)\n\n;; Read \"Free as in Freedom "
                     "2.0\" online from the GNU Press.\n(w3m-browse-url"
                     " \"https://www.fsf.org/faif/digital-copy-of-the-book-in-pdf-format\")"
                     "\n\n;; :stop_sign: end of rambling... I mean, *scratch*")))
                  (switch-to-buffer (get-buffer-create "*scratch*"))
                  (lisp-interaction-mode)
                  (unless (eq pegnumax-first-run 't) (turn-on-tempbuf-mode))
                  (blink-cursor-mode)
                  (emojify-mode)
                  (if (buffer-empty-p)
                      (insert initial-scratch-message))
                  (goto-char 0)
                  (buffer-disable-undo)
                  (buffer-enable-undo))

                ;; `powerline-moe-load' might be called by Emacs daemon; therefore, do not load twice.
                (when (not (daemonp)) (powerline-moe-load))

                ;; Setting up global minor modes, minor mode hooks, variables, etc.
                (show-paren-mode)
                (global-highlight-changes-mode +1)
                (add-hook 'prog-mode-hook
                          (lambda nil
                            (when (not (daemonp))
                              (unless (eq pegnumax-first-run 't) (linkd-mode)))
                            (linum-mode)
                            (beacon-mode)
                            (setq show-trailing-whitespace t)
                            (font-lock-add-keywords nil my-extra-keywords)))
                (add-hook 'emacs-lisp-mode-hook
                          (lambda nil
                            ;(highlight-defined-mode)
                            ;(highlight-quoted-mode)
                            ))
                (add-hook 'lisp-interaction-mode-hook ;; For *scratch*
                          (lambda nil
                            ;(highlight-defined-mode)
                            ;(highlight-quoted-mode)
                            (turn-on-pretty-mode)))
                (add-hook 'info-mode-hook (use-package info+ :unless noninteractive :ensure nil :load-path "elisp/"))

                ;; Move the mouse to the bottom-right corner of the screen on any keypress.
                (use-package avoid
                  :ensure nil
                  :demand t
                  :commands mouse-avoidance-mode
                  :config
                  (defun mouse-avoidance-banish-destination () (cons (+ 3 (frame-width)) (frame-height))))
                (mouse-avoidance-mode 'banish)

                ;; Launching customized *scratch* buffer and shell.
                ;; Take a look at an eshell config: (@url :file-name "https://github.com/dakra/dmacs/blob/master/init.org#eshell" :display "init.org#eshell").
                (with-eval-after-load "fancy-battery"
                  ;; (@* "Shell")
                  (shell)
                  (use-package wtf :ensure nil :commands (wtf-add wtf-get-term-at-point wtf-is wtf-remove))) ;; M-x wtf-is RET wtf
                (my-scratch-buffer)
                (make-org-ready) ;; (@> "Org Ready?") This has to be called twice for some reason (1st Call).
                (unless (eq pegnumax-first-run 't)
                  (unless (file-exists-p (concat elisp-dir "w3m-type-ahead.el"))
                    (wget "https://www.emacswiki.org/emacs/download/w3m-type-ahead.el")
                    (add-hook 'wget-after-hook 'compile-w3m-type-ahead))
                  (remove-output-buffer)
                  (delete-other-windows))))
    (add-hook 'emacs-startup-hook
              (lambda nil
                ;; Make sure there are no obsolete packages in `elpa-dir' before we start
                ;; The easiest way is to check for duplicates - see (@> "Obsolete Packages").
                (cd elpa-dir)
                (unless (string-equal (substring (shell-command-to-string "ls -1 | sed 's,-[0-9]*\.[0-9]*,,' | uniq -d | grep -v 'signed' | wc -l") 0 -1) "0")
                  (run-at-time "4.0 sec" nil 'package-delete-obsolete-packages))

                ;; The time consuming part is over.  The previous GC frequency and file name handling is back now.
                ;; Finished (@> "Garbage Collection").
                (setq gc-cons-threshold best-gc-cons-threshold)
                (setq file-name-handler-alist pegnumax--file-name-handler-alist)

                ;; If this is the first run of `PeGnuMax', then handle it accordingly.
                (when (eq pegnumax-first-run 't)
                  (switch-to-buffer "*emacs*")
                  (end-of-buffer)
                  (if (package-installed-p 'restart-emacs)
                      (if (yes-or-no-p "Welcome to `PeGnuMax'!  GNU Emacs will restart in ~20 seconds (currently compiling).  Is that OK? ")
                          (run-at-time "20.0 sec" nil 'restart-emacs))))

                ;; Since we're in Emacs, change `default-directory' to `user-emacs-directory'.
                (remove-output-buffer)
                (cd user-emacs-directory)

                ;; Report how well Emacs startup was.
                (message "Emacs ready in %s with %d garbage collections."
                         (format "%.2f seconds"
                                 (float-time
                                  (time-subtract after-init-time before-init-time)))
                         gcs-done)

                ;; Create a linkd button overlay in *scratch* for the (@> "Dashboard").
                (unless (eq pegnumax-first-run 't) (run-at-time "3.0 sec" nil 'linkd-mode))

                ;; `PeGnuMax' is initialized.
                (setq pegnumax-loading-p nil)))

    ;; I want the user to witness the package management process and `use-package' configurations, hence *Messages*.
    (switch-to-buffer "*Messages*")))

;; (@* "Pre-Package Management Settings")
(package-initialize nil)

;; `package--save-selected-packages' contains code allowing `customize' to edit `init.el'.
;; This is forbidden!  All `customize' settings are now organized inside `:custom*' tags.
(defun package--save-selected-packages (&rest _opt) nil)

;; Defining packages to be installed
(setq package-enable-at-startup nil ;; As of Emacs 27, this line should be in (@file :file-name "~/.emacs.d/early-init.el" :display "early-init.el").
      package-archives ;; Setup package repositories.
      '(("gnu"       . "https://elpa.gnu.org/packages/")
        ("melpa"     . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("melpa"     . 10)
        ("gnu"       . 05))
      package-selected-packages
      '(alarm-clock
        all-the-icons
        all-the-icons-dired
        amread-mode
        ascii-art-to-unicode
        auto-complete
        auto-package-update
        avy
        bbdb
        beacon
        benchmark-init
        bongo ;; (@info :file-name "bongo" :display "Play music with Emacs.")
        calibredb
        celestial-mode-line
        charmap
        chess ;; (@info :file-name "chess" :display "Chess.el is an Emacs chess client.")
        cl-libify
        csv-mode
        dashboard
        debbugs ;; (@info :file-name "debbugs" :display "A library for communication with Debbugs.")
        delight
        dimmer
        dired-du
        ;; `disk-usage' - Provided by GuixSD via package `emacs-disk-usage'
        ducpel
        e2wm ;; (@url :file-name "https://github.com/kiwanami/emacs-window-manager" :display "E2WM: Equilibrium Emacs Window Manager")
        easy-escape
        eev
        egg-timer
        elf-mode
        elfeed
        elisp-demos
        elpher ;; (@info :file-name "elpher" :display "A gopher and gemini client for Emacs.")
        emms
        emms-mode-line-cycle
        ;; `emstar' - Not available under GuixSD
        emojify
        engine-mode
        enwc ;; Requires either `wicd' or (@man :page "nm" :display "nm") - see (@> "Network Manager")
        ess ;; Required by `org-babel-eval-in-repl'
        esup
        eww-lnum
        exec-path-from-shell
        fancy-battery
        fill-column-indicator
        filladapt
        frog-jump-buffer
        gemini-mode
        greenbar
        gnu-elpa-keyring-update
        gnugo ;; (@info :file-name "gnugo" :display "The GNU Go program")
        guix
        helm
        helm-eww
        helm-system-packages ;; Requires system package: `recutils'
        helpful
        highlight-defined
        highlight-quoted
        htmlize
        image+
        jabber ;; (@info :file-name "jabber" :display "Emacs XMPP (Jabber) client")
        keypression
        lua-mode
        markdown-mode ;; Required by some (?) package
        ;; md4rd - Not available due to uninstallable packages (fix later)
        memoize ;; Required by `all-the-icons'
        messages-are-flowing
        metronome
        minesweeper
        mode-icons
        moe-theme
        mu-cite
        nov
        on-screen
        opener ;; (@info :file-name "opener" :display "Seamlessly open files via http(s) in buffer.")
        openwith
        org ;; Will be installed via `quelpa'
        org-babel-eval-in-repl
        ;; org-plus-contrib
        ;; org-timeline
        page-break-lines
        paredit ;; Required by `org-babel-eval-in-repl'
        pcmpl-args
        ;; `pdf-tools' - Provided by GuixSD via package `emacs-pdf-tools'
        ;; On other systems, requires packages: (libpng12-dev zlib1g-dev libpoppler-glib-dev libpoppler-private-dev)
        pdfgrep
        phi-grep
        php-mode
        pomidor
        powerline
        pretty-mode
        quelpa ;; Required to build my preferred version of `w3m'
        quelpa-use-package
        rainbow-delimiters
        restart-emacs
        ;; `rtf-mode' provided by `elisp-dir' "rtf-mode.el"
        sed-mode
        spray ;; For (@> "Speed Reading")
        sudo-edit
        sudoku
        ;; sunshine - There's a bug with this program
        svg-clock
        svg-tag-mode
        system-packages
        tablist ;; Required by GNU Store
        threes ;; Requires obsolete package `seq-2.20'
        tldr
        tramp-theme
        transcribe
        typo
        uimage
        unicode-fonts
        use-package ;; (@info :file-name "use-package" :display "Declarative package configuration for Emacs.")
        use-package-ensure-system-package
        ;; `visual-basic-mode' provided by (@file :file-name "~/.emacs.d/elisp/visual-basic-mode.el" :display "visual-basic-mode.el")
        visual-fill-column ;; for use with `gemini-mode'
        volatile-highlights
        volume
        ;; `w3m' - Will be installed via `quelpa'
        wanderlust ;; (@info :file-name "wanderlust" :display "Yet Another Message Interface On Emacsen")
        wc-mode
        wget
        wgrep
        which-key
        window-number
        wttrin
        xpm
        ytel
        ;; `zoom-frm' provided by (@file :file-name "~/.emacs.d/elisp/frame-cmds.el" :display "frame-cmds.el"), (@file :file-name "~/.emacs.d/elisp/frame-fns.el" :display "frame-fns.el"), and (@url :file-name "~/.emacs.d/elisp/zoom-frm.el" :display "zoom-frm.el") inside of `elisp-dir'
        ))
;; (@* "Load Path")
;; Setup basic load path and other lists.
(add-to-list 'load-path elpa-dir)
(add-to-list 'load-path elisp-dir)
(add-to-list 'load-path (concat elisp-dir "elim/elisp")) ;; For (@> "ELIM")
(add-to-list 'custom-theme-load-path (expand-file-name (concat user-emacs-directory "themes/")))
(if (eq pegnumax-first-run 't)
    (progn
      ;; Let's create a bash script to help the user launch `PeGnuMax' from a daemon (for those who want to).
      (unless (file-exists-p (concat user-emacs-directory "pegnumax.sh"))
        (defvar pegnumax-string
          (concat
           "echo -e %s > " user-emacs-directory "pegnumax.sh && chmod +x " user-emacs-directory "pegnumax.sh")
          "Provided as an argument to `shell-command' to create the `pegnumax-script'.")
        (defvar pegnumax-script
          (concat
           "# -*- shell-script -*-\\n"
           "# pegnumax.sh -- Starts the GNU Emacs server (launched as daemon).\\n"
           "#\\n"
           "# Copyright (C) 2019 Michael Pagan (pegzmasta) <michael.pagan@member.fsf.org>\\n"
           "#\\n"
           "# This program can be called by a GNOME keyboard shortcut: bash -c '~/.emacs.d/pegnumax.sh' > /dev/null &2>&1\\n"
           "# My GNOME keyboard shortcut is bound to the key [XF86Launch1], and you might want to do the same/similar.\\n"
           "nice -40 emacsclient -c -a '' -e '(powerline-moe-load)'") ;; Possible on GuixSD
          "A Shell script to help the user launch GNU Emacs with a press of a button on the keyboard.")
        (shell-command (format pegnumax-string (shell-quote-argument pegnumax-script))))
      (setq package-check-signature nil)))

;; (@* "Package Authentication")
;; Download the GNU ELPA keyring for proper package signature verification.
(unless (package-installed-p 'gnu-elpa-keyring-update)
  (setq package-gnupghome-dir "~/.gnupg/")
  (with-temp-buffer
    (if (eq buffer-read-only 't) (read-only-mode -1))
    (url-insert-file-contents "http://git.savannah.gnu.org/cgit/emacs/elpa.git/plain/gnu-elpa-keyring-update.el?h=externals/gnu-elpa-keyring-update")
    (package-install-from-buffer)
    (kill-buffer)
    (make-directory (concat elpa-dir "gnu-elpa-keyring-update-2019.3/etc"))
    (find-file (concat elpa-dir "gnu-elpa-keyring-update-2019.3/etc/gnu-elpa.gpg-keyring"))
    (url-insert-file-contents "http://git.savannah.gnu.org/cgit/emacs/elpa.git/plain/etc/gnu-elpa.gpg-keyring?h=externals/gnu-elpa-keyring-update")
    (save-buffer)
    (kill-buffer)
    (gnu-elpa-keyring-update)))

;; (@* "Package Manager")
(eval-and-compile
  (unless package-archive-contents (package-refresh-contents))
  (unless (eq pegnumax-first-run 't)
    (autoload 'turn-on-tempbuf-mode "tempbuf" t)
    (setq quelpa-build-verbose nil
          quelpa-update-melpa-p nil
          quelpa-checkout-melpa-p nil))
  (unless (package-installed-p 'quelpa)
    (with-temp-buffer
      (url-insert-file-contents "https://github.com/quelpa/quelpa/raw/master/quelpa.el")
      (eval-buffer)
      (quelpa-self-upgrade)))
  (quelpa '(quelpa-use-package :fetcher git :url "https://github.com/quelpa/quelpa-use-package.git") :upgrade nil)
  ;; Otherwise, Emacs complains with `void-variable' or `void-function'
  (defvar use-package-statistics (make-hash-table))
  (defun use-package-statistics-gather (keyword name after)
    (let* ((hash (gethash name use-package-statistics
                          (make-hash-table)))
           (before (and after (gethash keyword hash (current-time)))))
      (puthash keyword (current-time) hash)
      (when after
        (puthash (intern (concat (symbol-name keyword) "-secs"))
                 (time-subtract (current-time) before) hash))
      (puthash name hash use-package-statistics))))

;; `use-package' essentials
(use-package benchmark-init :demand t :hook (after-init . benchmark-init/deactivate))
(use-package cl-lib)
(use-package bind-key ;; required per (@url :file-name "https://github.com/jwiegley/use-package/issues/991" :display ":bind* not working anymore? #991")
  :config
  (setq override-global-mode t))
(use-package quelpa-use-package)

;; (@* "Ensure Packages")
(use-package use-package-ensure :ensure nil :config (setq use-package-always-ensure t))
(use-package use-package-ensure-system-package
  :commands  use-package-ensure-system-package-exists?
  :init
  (use-package exec-path-from-shell
    :commands  exec-path-from-shell-initialize
    :custom
    ;; Ensure shell execution takes less time than `exec-path-from-shell-warn-duration-millis'
    (exec-path-from-shell-arguments nil)
    (exec-path-from-shell-variables nil)
    :init
    ;; (@* "Environment") - These variables must be set _before_ shell execution.
    ;; `bashrc' is no longer required in order to setup the environment.
    ;; `bashrc' still houses my aliases, functions, and other routines.
    (setenv "PAGER" "cat")
    (setenv "CVS_RSH" "ssh")
    (setenv "EDITOR" "emacsclient -c -a \"\" \"$@\"")
    ;; (@> "Ensure C Compiler is found"), since I have no symbolic link for `gcc'
    (unless (executable-find "cc") (setenv "CC" "gcc"))
    (setenv "HISTSIZE" "1000")
    (setenv "HISTFILESIZE" "1000")
    (setenv "HISTCONTROL" "ignoredups")
    ;; (setenv "PS1" "[\\u@\\h | \\#/\\!]$ ")
    ;; (@> "PS1") - Fancy shell prompt
    (setenv "PS1" "\\e[35;1m\[\\e[32;1m\\u\\e[33;41m@\\h:\\e[0m\\e[36;1m\\011(D$(ls -l | grep -v total | grep '^d' | wc -l) F$(ls -l | grep -v total | grep '^-' | wc -l) L$(ls -l | grep -v total | grep '^l' | wc -l))-(#\\#)\\011\\e[32;1m\\w\\e[0m\\e[35;1m]-$ \\e[0m")
    (setenv "TMPDIR" (concat (getenv "HOME") "/tmp"))
    (setenv "WWW_HOME" "about:")
    ;; (setenv "WWW_HOME" "https://html.duckduckgo.com/")
    (setenv "GAME" (concat (getenv "HOME") "/Code/games"))
    (setenv "CVSROOT" (concat (getenv "HOME") "/Code/cvs-projects"))
    (setenv "PKG_CONFIG_PATH" (concat (getenv "PKG_CONFIG_PATH") ":/usr/local/lib/pkgconfig"))
    (setenv "JAVA_HOME"
            (substring
             (shell-command-to-string
              (concat "dirname $(dirname $(readlink -f $(which javac)))")) 0 -1))
    (setenv "PATH" (concat "/bin:"
                           (getenv "GAME") ":"
                           (getenv "PATH") ":"
                           (getenv "JAVA_HOME")))
    (setenv "CDPATH" (concat (getenv "HOME") ":"
                             (getenv "HOME") "/Code:"
                             (getenv "HOME") "/Desktop:"
                             (getenv "HOME") "/Documents:"
                             (getenv "HOME") "/Downloads:"
                             (getenv "HOME") "/Movies:"
                             (getenv "HOME") "/Music:"
                             (getenv "HOME") "/Pictures:"
                             (getenv "HOME") "/Videos"))
    (exec-path-from-shell-initialize)))

;; (@* "Package Updater")
(use-package auto-package-update
  :demand    t
  :commands  auto-package-update-at-time
  :hook     (auto-package-update-after-hook . package-autoremove)
  :custom
  (auto-package-update-delete-old-versions t)
  (auto-package-update-hide-results        t)
  (auto-package-update-interval            1) ;; Update Emacs once a day.
  :config
  (defun package-delete-obsolete-packages nil ;; (@> "Obsolete Packages")
    ;; When older versions of a package accumulate in `elpa-dir', this creates
    ;; duplicate packages which act as false positives for tests on various
    ;; directories of Emacs packages.  These duplicates are no longer required
    ;; and should be deleted!
    (interactive)
    (save-window-excursion
      (list-packages)
      (package-menu-mark-obsolete-for-deletion)
      (package-menu-execute :no-query)
      (kill-this-buffer)))
  (advice-add 'package-autoremove :around #'my/disable-yornp)
  (auto-package-update-at-time "21:00"))

;; (@* "System Package Manager")
;; Bug Report: (@url :file-name "https://issues.guix.gnu.org/44606" :display "Error with `guix-start-repl', No prompt found")
(use-package guix
  :ensure-system-package (guile guix)
  :bind* ("C-x C-g" . guix)
  :preface
  (defvar glibc-locales-dir
    (substring
     (shell-command-to-string
      (concat "dirname /gnu/store/*glibc-locales*/. | grep -v \\*")) 0 -1))
  :init
  (if (eq glibc-locales-dir nil)
      (system-packages-install "glibc-locales"))
  ;; Workaround: (@url :file-name "https://github.com/alezost/guix.el/issues/39#issuecomment-712413400" :display "Can't run guix commands #39")
  (defun guix-buffer-p (&optional buffer)
    (let ((buf-name (buffer-name (or buffer (current-buffer)))))
      (not (null (or (string-match "*Guix REPL" buf-name)
                  (string-match "*Guix Internal REPL" buf-name))))))
  (defun guix-geiser--set-project (&optional _impl _prompt)
    (when (and (eq 'guile geiser-impl--implementation)
             (null geiser-repl--project)
             (guix-buffer-p))
      (geiser-repl--set-this-buffer-project 'guix)))
  (advice-add 'geiser-impl--set-buffer-implementation :after #'guix-geiser--set-project))
(use-package helm-system-packages :ensure-system-package (recsel . recutils) :defer t)

;; (@* "Network Manager")
(use-package enwc
  :disabled ;; Does not work for Emacs 28.1
  :commands  enwc
  :custom
  (enwc-default-backend 'nm "Network Manager")
  (enwc-wired-device    "enp0s25")
  (enwc-wireless-device "wlp2s0")
  (enwc-force-backend-loading t)
  :init
  ;; Ignore: "Error while verifying signature enwc"
  ;; It's actually a "Good signature" - marked: [unknown]
  (if (get-buffer  "*Error*")
      (kill-buffer "*Error*")))

;; (@* "GNU Downloader")
;; (@info :file-name "wget" :display "Wget")
(use-package wget
  :ensure nil
  :ensure-system-package (wget git)
  :quelpa ((wget :fetcher git :url "https://github.com/ataka/emacs-wget") :upgrade nil) ;; Required for GuixSD
  :custom
  (wget-download-directory-filter #'wget-download-dir-filter-regexp)
  ;; (@* "Download Directory REGEX")
  (wget-download-directory
   `(("\\.el$"                                                         . ,elisp-dir)
     ("\\.\\(asc\\|bundle\\|scm\\|gz\\|lz\\|xz\\|bz2\\|tgz\\|sh\\|jar\\|deb\\|gpg\\|sig\\|patch\\)$" . "~/Code")
     ("\\.\\(love\\|rpy?\\)$"                                                . "~/Code/games")
     ("\\.\\(oga\\|ogg\\|mogg\\|mp3\\|flac\\)$"                                    . "~/Music")
     ("\\.\\(?:mpe?g\\|mp4\\|mov\\|mkv\\|flv\\|ogv\\|webm\\|avi\\|wmv\\|ts\\)$"              . "~/Videos")
     ("\\.\\(jp?g\\|png\\|gif\\)$"                                             . "~/Pictures")
     ("\\.\\(epub\\|md\\|pdf\\|tex\\|txt\\)$"                                      . "~/Documents")
     (".*"                                                            . "~/Downloads")))
  ;; Let's download some elisp files which I've found handy.
  :commands wget-download-dir-filter-regexp
  :init
  (unless (file-directory-p "~/Code/games") (make-directory "~/Code/games"))
  (unless (file-directory-p elisp-dir)
    (mkdir elisp-dir)
    (dolist (pkg '("bookmark+"
                   "bookmark+-mac"
                   "bookmark+-bmu"
                   "bookmark+-1"
                   "bookmark+-key"
                   "bookmark+-lit"
                   "frame-cmds"
                   "frame-fns"
                   "grep+"
                   "info+"
                   "lua-block"
                   "re-builder+"
                   "rtf-mode"
                   "tempbuf"
                   "visual-basic-mode"
                   "zoom-frm"))
      (unless (file-exists-p (concat elisp-dir pkg ".el"))
        (defvar wget-emacswiki "https://www.emacswiki.org/emacs/download/"
          "Some packages can only be found here.")
        (wget (concat wget-emacswiki pkg ".el"))))
    ;; The latest unicode data
    (wget "https://raw.githubusercontent.com/emacsmirror/dired-plus/master/dired+.el") ;; As of 20 July 2022, this is the best version.
    (wget "https://www.unicode.org/Public/UCD/latest/ucd/UnicodeData.txt")
    (wget "https://github.com/michaelrsweet/mxml/releases/download/v3.2/mxml-3.2.tar.gz") ;; (@> "Mini-XML")
    (wget "https://freenetproject.org/assets/keyring.gpg")
    (wget "https://github.com/freenet/fred/releases/download/build01488/new_installer_offline_1488.jar")
    (wget "https://github.com/freenet/fred/releases/download/build01488/new_installer_offline_1488.jar.sig")
    ;; On gopherspace now - see (@> "Sixel")
    ;; (wget "https://raw.githubusercontent.com/tgvaughan/sixel/master/sixel.el")
    (wget "http://mwolson.org/static/dist/elisp/wtf.el")
    (wget "https://download.tuxfamily.org/user42/sqlite-dump.el")
    (wget "https://alexschroeder.ch/cgit/gopher.el/plain/gopher.el")
    (wget "https://alexschroeder.ch/cgit/gemini-write/plain/gemini-write.el")
    (wget "https://raw.githubusercontent.com/balle/dired-mtp/master/dired-mtp.el")
    (wget "http://www.pps.univ-paris-diderot.fr/~jch/software/files/format-flowed.el")
    (wget "https://raw.githubusercontent.com/tecosaur/screenshot/master/screenshot.el")
    (wget "https://web.archive.org/web/20080403111240/http://dto.mamalala.org/eon/usb.el")
    (wget "https://web.archive.org/web/20080403111136/http://dto.mamalala.org/eon/radio.el")
    ;; (@> "Games")
    ;; Rogue-Like eXtended game engine for Emacs (make sure to `cl-libify' the file)
    ;; This type of game uses Common Lisp; therefore, a compiler is needed - (@url :file-name "http://sbcl.org/" :display "SBCL").
    ;; Learn more here: (@url :file-name "https://web.archive.org/web/20130312104929/http://dto.github.com/notebook/rlx.html" :display "http://dto.github.com/notebook/rlx.html")
    ;; Modern version - (@url :file-name "http://xelf.me/" :display "eXtensible Emacs-Like Facility")
    ;; (wget "https://web.archive.org/web/20080403111029/http://dto.mamalala.org/eon/rlx.el")
    (wget "https://gist.githubusercontent.com/dto/4112806/raw/6ecd8f4a16f6b8edbd1289d7ef5569ad8a6fc247/rlx.el")
    ;; CHIP-8 emulator for Emacs - (@url :file-name "https://depp.brause.cc/chip8.el/" :display "chip8.el")
    (wget "https://depp.brause.cc/chip8.el/chip8.el")
    (wget "https://depp.brause.cc/chip8.el/chip8-test.el")
    ;; Eboy - (@url :file-name "https://github.com/vreeze/eboy" :display "Emacs Gameboy Emulator")
    ;; Make sure to check (@url :file-name "https://github.com/vreeze/eboy/blob/master/eboy-notes.org" :display "eboy-notes.org").
    (wget "https://raw.githubusercontent.com/vreeze/eboy/master/eboy-cpu.el")
    (wget "https://raw.githubusercontent.com/vreeze/eboy/master/eboy-macros.el")
    (wget "https://raw.githubusercontent.com/vreeze/eboy/master/eboy.el")
    ;; NES emulator for Emacs - see (@url :file-name "https://github.com/gongo/emacs-nes" :display "emacs-nes")
    (wget "https://raw.githubusercontent.com/gongo/emacs-nes/master/nes-cartridge.el")
    (wget "https://raw.githubusercontent.com/gongo/emacs-nes/master/nes-color.el")
    (wget "https://raw.githubusercontent.com/gongo/emacs-nes/master/nes-cpu.el")
    (wget "https://raw.githubusercontent.com/gongo/emacs-nes/master/nes-dma.el")
    (wget "https://raw.githubusercontent.com/gongo/emacs-nes/master/nes-instruction.el")
    (wget "https://raw.githubusercontent.com/gongo/emacs-nes/master/nes-interrupt.el")
    (wget "https://raw.githubusercontent.com/gongo/emacs-nes/master/nes-keypad.el")
    (wget "https://raw.githubusercontent.com/gongo/emacs-nes/master/nes-ppu.el")
    (wget "https://raw.githubusercontent.com/gongo/emacs-nes/master/nes-util.el")
    (wget "https://raw.githubusercontent.com/gongo/emacs-nes/master/nes.el")))

(use-package all-the-icons-dired
  :unless noninteractive
  ;; (@* "Fonts")
  :init
  (unless (find-font (font-spec :name "Hack"))
    (use-package unicode-fonts
      :ensure-system-package (font-hack font-gnu-unifont font-gnu-freefont)
      :config
      (unicode-fonts-setup)
      (unless (file-exists-p (concat user-emacs-directory "UnicodeData.txt"))
        (rename-file "~/Downloads/UnicodeData.txt" user-emacs-directory))))
  (use-package unicode-fonts
    :init
    (setq describe-char-unicodedata-file (concat user-emacs-directory "UnicodeData.txt"))
    ;; Set default file encoding.
    (set-language-environment "UTF-8")
    (prefer-coding-system 'utf-8)
    (set-terminal-coding-system 'utf-8)
    (set-default-coding-systems 'utf-8)
    (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))
  (if (eq pegnumax-first-run 't) (all-the-icons-install-fonts))
  :after celestial-mode-line
  ;; (@* "Dired Icons") - These are for dired.  Another package provides (@> "Mode Icons") for the modeline.
  :custom
  (all-the-icons-dired-monochrome nil)
  :config
  ;; Adding new associations for different file types.
  (setq all-the-icons-icon-alist (append '(("\\.data$" all-the-icons-faicon "bar-chart" :face all-the-icons-cyan :height 0.9)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("^COPYING.*$" all-the-icons-octicon "book" :height 1.0 :v-adjust 0.0 :face all-the-icons-blue)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("^HACKING" all-the-icons-octicon "book" :height 1.0 :v-adjust 0.0 :face all-the-icons-lblue)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("^INSTALL" all-the-icons-octicon "book" :height 1.0 :v-adjust 0.0 :face all-the-icons-lblue)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("^THANKS" all-the-icons-octicon "book" :height 1.0 :v-adjust 0.0 :face all-the-icons-lblue)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("^NEWS" all-the-icons-octicon "book" :height 1.0 :v-adjust 0.0 :face all-the-icons-purple)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.ics$" all-the-icons-octicon "calendar" :v-adjust 0.0 :face all-the-icons-green)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.diff$" all-the-icons-octicon "git-compare" :v-adjust 0.0 :face all-the-icons-red)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.patch$" all-the-icons-octicon "git-compare" :v-adjust 0.0 :face all-the-icons-red)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.db$" all-the-icons-octicon "database" :height 1.0 :v-adjust 0.0 :face all-the-icons-green)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.bblx$" all-the-icons-octicon "database" :height 1.0 :v-adjust 0.0 :face all-the-icons-green)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.sqlite$" all-the-icons-octicon "database" :height 1.0 :v-adjust 0.0 :face all-the-icons-green)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.tgz$" all-the-icons-octicon "file-binary" :v-adjust 0.0 :face all-the-icons-lmaroon)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.bz2$" all-the-icons-octicon "file-zip" :v-adjust 0.0 :face all-the-icons-lgreen)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.xsl$" all-the-icons-faicon "file-code-o" :height 0.95 :face all-the-icons-lorange)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.opf$" all-the-icons-faicon "file-code-o" :height 0.95 :face all-the-icons-lgreen)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.xcf$" all-the-icons-octicon "file-media" :v-adjust 0.0 :face all-the-icons-cyan)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.ps" all-the-icons-octicon "file-text" :v-adjust 0.0 :face all-the-icons-orange)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.rtf" all-the-icons-octicon "file-text" :v-adjust 0.0 :face all-the-icons-purple)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.mkv" all-the-icons-faicon "film" :face all-the-icons-dorange)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.flv" all-the-icons-faicon "film" :face all-the-icons-lgreen)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.webm" all-the-icons-faicon "film" :face all-the-icons-orange)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.epub$" all-the-icons-alltheicon "html5" :face all-the-icons-lblue)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.jar$" all-the-icons-alltheicon "java" :height 1.0 :face all-the-icons-purple)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.asc$" all-the-icons-octicon "key" :v-adjust 0.0 :face all-the-icons-cyan)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.awk" all-the-icons-alltheicon "terminal" :face all-the-icons-cyan)) all-the-icons-icon-alist)
        all-the-icons-icon-alist (append '(("\\.sed" all-the-icons-alltheicon "terminal" :face all-the-icons-lmaroon)) all-the-icons-icon-alist)))

;; Add packages to `pkg' list below if you experience errors during installation due to certain dependencies,
;; Emacs version, "Bad Request," untrusted signature, etc.  Obsolete packages go here, as well. You may
;; install them after `package-install-selected-packages' has executed via the `use-package' ensure mechanism.
(dolist
    (pkg '(dashboard
           elpher
           enwc
           eww-lnum
           fill-column-indicator
           frog-jump-buffer
           keypression
           gnugo
           markdown-mode
           ;; org-plus-contrib
           phi-grep
           wanderlust
           ytel)) ;; This list of packages may continue to grow; don't scoff at it!
  (unless (package-installed-p pkg)
    (setq package-selected-packages (delete pkg package-selected-packages))))

;; (@* "Clients For Different Network Protocols")
;; Ask whether we'll browse webpages from within a buffer or an external browser.
(defun choose-browser (url &rest _args)
  (interactive "sURL: ")
  (with-no-warnings
    (if (y-or-n-p (concat "URL: " url "\nUse external browser (Chromium)? "))
        (browse-url-generic url)
      (if (y-or-n-p (concat "URL: " url "\nUse W3M? "))
          (w3m-browse-url url)
        (if (y-or-n-p (concat "URL: " url "\nUse EWW? "))
            (eww url)
          (if (y-or-n-p (concat "URL: " url "\nUse Lynx? "))
              (progn
                (setenv "WWW_HOME" url)
                (lynx)
                (setenv "WWW_HOME" "about:"))
            (if (y-or-n-p (concat "URL: " url "\nUse Gopher? "))
                (if (not (version< emacs-version "26.2"))
                    (elpher-go url)
                  (gopher url))
              (if (y-or-n-p (concat "URL: " url "\nUse MPV/VLC? "))
                  (progn
                    (use-package openwith)
                    (declare-function openwith-open-unix "openwith.el" (COMMAND ARGLIST))
                    (openwith-open-unix (concat "mpv --profile=big-cache " url " || cvlc --play-and-exit " url) nil)))
              ;; Open inside an Emacs buffer (good for text files and source code).
              (defvar opener-dir
                (substring (shell-command-to-string (concat "ls " elpa-dir " | grep 'opener-[0-9]*\.[0-9]*$'")) 0 -1))

              ;; Bugfix - prevents invalid function error
              (when (equal "function*\n" (shell-command-to-string (concat "grep -o 'function\\*' " elpa-dir "opener-*/opener.el")))
                (if (file-exists-p (concat elpa-dir opener-dir "/opener.elc"))
                    (delete-file (concat elpa-dir opener-dir "/opener.elc")))
                (shell-command (concat "sed -i 's,function\\*,cl-function,' " elpa-dir opener-dir "/opener.el"))
                (byte-compile-file (concat elpa-dir opener-dir "/opener.el")))
              (opener-open url t))))))))

;; (@* "Multimedia")
(use-package openwith
  :unless noninteractive
  :ensure-system-package (file antiword gthumb)
  :commands  openwith-mode
  :hook (dired-mode . openwith-mode)
  :custom
  (openwith-confirm-invocation t)
  (openwith-associations
   '(("\\.doc\\'" "antiword" (file))
     ("\\.mp3\\'" "mpv" (file))
     ("\\.\\(?:mpe?g\\|mp4\\|mov\\|mkv\\|flv\\|ogg\\|ogv\\|webm\\|avi\\|wmv\\|ts\\)\\'" "mpv" ("--profile=big-cache" file))
     ("\\.m3u\\'" "mpv" ("--playlist" file))
     ("\\.gif\\'" "animate" (file))
     ("\\.webp\\'" "gthumb" (file)))))
(use-package transcribe :ensure-system-package mpg123 :defer t)
(use-package bongo
  :ensure-system-package (mpg123 mpv youtube-dl)
  :defer t
  :config (setq bongo-enabled-backends (quote (mpg123 mpv)))) ;; GNU MPV not fully supported
(use-package emms
  :defer t
  :config
  (use-package emms-setup :ensure nil)
  (use-package emms-player-mpv
    :ensure nil
    :after     emms-setup
    :config
    (setq emms-player-list '(emms-player-mpv))
    (setq emms-player-mpv-parameters '("--no-terminal" "--force-window=no" "--audio-display=no")))
  (setq emms-player-list '(emms-player-mpv))
  ;; Minor mode for updating `emms-mode-line-string' cyclically within specified width
  (use-package emms-mode-line-cycle
    :preface
    (declare-function emms-mode-line "emms-mode-line.el" (ARG))
    (declare-function emms-playing-time "emms-playing-time.el" (ARG))
    (declare-function emms-mode-line-cycle "emms-mode-line-cycle.el" (&optional ARG))
    :config
    (emms-mode-line 1)
    (emms-playing-time 1)
    ;; `emms-mode-line-cycle' can be used with emms-mode-line-icon.
    (use-package emms-mode-line-icon :ensure nil)
    (emms-mode-line-cycle 1)))

(unless (version< emacs-version "25.3")
  (use-package ytel ;; Experimental YouTube "frontend" for Emacs
    :disabled ;; As of (@url :file-name "https://github.com/emacs-w3m/emacs-w3m/commit/6ed57faedf7b0459c41438bf2077a7ff4946e742" :display "Sep 15, 2020"), YouTube Search now fails due to JavaScript implementation :-(.
    :ensure-system-package (curl youtube-dl)
    :preface
    (eval-when-compile (declare-function ytel-get-current-video "ytel.el"))
    (defun ytel-watch ()
      "Stream video at point in mpv."
      (interactive)
      (let* ((video (ytel-get-current-video))
             (id    (ytel-video-id video)))
        (start-process "ytel mpv" nil
                       "mpv"
                       "--profile=big-cache"
                       (concat "https://www.youtube.com/watch?v=" id))
        "--ytdl-format=bestvideo[height<=?720]+bestaudio/best")
      (message "Starting streaming..."))
    :bind (:map ytel-mode-map ("<return>" . ytel-watch))))

;; SQL - Structured Query Language
(autoload 'sqlite-dump "sqlite-dump" nil t)
(defalias 'sql-get-login 'ignore)
(modify-coding-system-alist 'file "\\.sqlite\\'" 'raw-text-unix)
(add-to-list 'auto-mode-alist '("\\.sqlite\\'" . sqlite-dump))

;; For (@> "Speed Reading")
(use-package amread-mode
  :ensure t
  :commands (amread-mode))

;; (@* "eBook Readers")
(use-package nov
  :defer t
  :config
  (setq nov-text-width t)
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))
(use-package calibredb
  :defer t
  :ensure-system-package (calibre (sqlite3 . sqlite))
  :config
  (setq sql-sqlite-program (substring (shell-command-to-string "which sqlite3") 0 -1)
        calibredb-root-dir "~/Calibre Library/"
        calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir)
        calibredb-program (substring (shell-command-to-string "which calibre") 0 -1)
        calibredb-format-all-the-icons t)
  (defadvice calibredb-find-file (after render-epub nil activate) (nov-mode)))

;; PDF Reader - Provided by GuixSD via package `emacs-pdf-tools'
(use-package pdf-tools ;; Open PDF files with `pdf-tools', instead of DocView mode.
  :ensure    nil
  :ensure-system-package (autoconf automake make (makeinfo . texinfo))
  :defer     t
  :unless    noninteractive
  :preface  (declare-function pdf-occur-global-minor-mode "pdf-occur.el" (&optional ARG))
  :commands  pdf-tools-install
  :magic  ("%PDF" . pdf-view-mode)
  :init
  (advice-add 'pdf-tools :around #'suppress-messages)
  :after celestial-mode-line
  :config
  ;; Prevent this variable from shadowing `outline-forward-same-level`
  (unless (boundp 'pdf-info-check-epdfinfo) (pdf-tools-install :no-query))
  (setq-default pdf-view-display-size 'fit-height)
  (use-package             pdfgrep
    :ensure-system-package pdfgrep
    :commands              pdfgrep-mode))

;; (@* "Feed Readers")
;; Snippet source: (@url :file-name "https://github.com/jeko2000/.emacs.d/blob/master/settings/config.org#elfeed" :display "config.org#elfeed")
(use-package elfeed
  :defer t
  :preface
  (declare-function elfeed-make-tagger "elfeed.el"
                    (FEED-TITLE FEED-URL ENTRY-TITLE ENTRY-LINK))
  :bind ("C-x F" . 'elfeed)
  :config
  (defmacro elfeed-add-face (face spec doc &rest args)
    "Create a new face elfeed-FACE and push it to the `elfeed-search-face-alist'."
    (declare (indent 0))
    (let ((elfeed-face-symbol
           (intern (concat "elfeed-" (symbol-name face)))))
      `(progn
         (defface ,elfeed-face-symbol
           ,spec
           ,doc
           ,@ args)
         (push '(,face ,elfeed-face-symbol)
               elfeed-search-face-alist))))
  (setq elfeed-feeds
        '(("https://estamosaquitravel.com/feed.atom" travel estamosaqui)
          ("http://endlessparentheses.com/atom.xml" tech emacs)
          ("http://planet.emacsen.org/atom.xml" tech emacs)
          ("http://nullprogram.com/feed/" tech)
          ("https://felixcrux.com/blog/rss.xml" tech)
          ("https://www.xkcd.com/atom.xml" webcomic)
          ("http://nedroid.com/feed/" webcomic)
          ("https://pthree.org/feed" blog)
          ("http://esr.ibiblio.org/?feed=rss2" blog)
          ("http://blog.cryptographyengineering.com/feeds/posts/default" blog)
          ("http://accidental-art.tumblr.com/rss" image math)
          ("https://www.npr.org/rss/podcast.php?id=510299" audio)
          ("http://english.bouletcorp.com/feed/" comic)
          ("http://bit-player.org/feed" blog math)
          ("http://simblob.blogspot.com/feeds/posts/default" blog dev)
          ("https://utcc.utoronto.ca/~cks/space/blog/?atom" blog dev)
          ("http://www.commitstrip.com/en/feed/" comic dev)
          ("http://feeds.feedburner.com/Buttersafe" comic)
          ("http://feeds.feedburner.com/CatVersusHuman" comic)
          ("http://feeds.feedburner.com/channelATE" comic)
          ("https://lemire.me/blog/feed/" dev blog)
          ("https://danluu.com/atom.xml" dev blog)
          ("https://www.blogger.com/feeds/19727420/posts/default" blog)
          ("https://dendibakh.github.io/feed.xml" blog dev)
          ("https://drewdevault.com/feed.xml" blog dev)
          ("http://dvdp.tumblr.com/rss" image)
          ("https://www.digitalocean.com/blog/feed" blog product)
          ("http://bay12games.com/dwarves/dev_now.rss" blog gaming product)
          ("http://danwang.co/feed/" blog philosophy)))
  (setq url-queue-timeout 20)
  (add-hook 'elfeed-new-entry-hook (elfeed-make-tagger :before "2 weeks ago" :remove 'unread))

  ;; Faces
  (elfeed-add-face audio
                   '((t :foreground "#FA0"))
                   "Marks podcasts in Elfeed."
                   :group 'elfeed)
  (elfeed-add-face webcomic
                   '((t :foreground "#BFF"))
                   "Marks webcomics in Elfeed."
                   :group 'elfeed))

;; (@url :file-name "https://github.com/ahungry/md4rd" :display "md4rd - Mode for Reddit")
(use-package md4rd ;; (@> "Mode for Reddit")
  :disabled
  :defer t
  ;; The original keybinding was overwritten by `md4rd-upvote'
  :bind (:map md4rd-mode-map ("U" . tree-mode-goto-parent))
  :config
  (setq md4rd-subs-active '(gnu
                            hurd
                            Trisquel
                            bash
                            commandline
                            emacs
                            orgmode
                            lisp+Common_Lisp
                            prolog
                            unixporn
                            anime
                            Animemes
                            TheRightCantMeme
                            MandelaEffect
                            vegan
                            nutrition
                            GNUnet
                            Freenet
                            i2p
                            i2pd
                            tor
                            onions
                            deepweb
                            GnuPG
                            cybersecurity
                            cryptocurrency
                            btc
                            Bitcoin
                            lbry)))
;; (@* "Web Browsers")
;; Our Internet browsers of choice (including an external web browser)
(if (not (executable-find "chromium"))
    (system-packages-install "ungoogled-chromium")
  (setq browse-url-browser-function 'choose-browser url-http-attempt-keepalives nil
        ;; browse-url-generic-program "nyxt")   ;; Not mature enough
        ;; browse-url-generic-program "icecat") ;; Gnuzilla/LibreJS bug prevents JavaScript execution.
        browse-url-generic-program "chromium"))

;; (@* "Lynx") - The text web browser
(defun lynx nil
  (interactive)
  (unless (executable-find "lynx")
    (system-packages-install "lynx"))
  (if (string-equal (getenv "WWW_HOME") "about:")
      (setenv "WWW_HOME"))
  (unless (file-exists-p "~/.lynxrc")
    (shell-command "echo -e 'DEFAULT_EDITOR:emacsclient\nEMACS_KEYS_ALWAYS_ON:TRUE\nFORCE_SSL_COOKIES_SECURE:TRUE\nPRETTYSRC:TRUE\nTAGSOUP:TRUE' \>\> ~/.lynxrc"))
  (setenv "LYNX_CFG" (concat (getenv "HOME") "/.lynxrc"))
  (ansi-term "lynx" "Lynx"))

;; (@* "Gopher")
(if (version< emacs-version "26.2")
    (use-package gopher
      :ensure nil
      :commands  gopher
      :hook     (gopher-mode-hook . (lambda nil (setq show-trailing-whitespace nil))))
  (use-package gopher :disabled)

  ;; Inherits from org-mode faces by default... yay!
  (use-package  elpher
    :commands  (elpher elpher-go)
    :hook      (elpher-mode . make-org-ready) ;; (@> "Org Ready?") If Org is not loaded, do so now.
    :bind (:map elpher-mode-map ;; There exists various program types in gopherspace, hence these keybindings.
                ("C-c o" . org-mode)
                ("C-c c" . conf-mode)
                ;; ("C-c g" . gemini-mode)
                ("C-c x" . makefile-mode)
                ("C-c m" . markdown-mode)
                ("C-c e" . emacs-lisp-mode)
                ("C-c s" . shell-script-mode)
                ("C-c 6" . sixel-render-images-in-buffer))
    :bind* ("C-c r" . elpher-redraw) ;; Return to `elpher-mode-map'.
    :custom-face
    (elpher-margin-key
     ((t
       (:inherit org-tag
                 :inverse-video t
                 :background "blue"
                 :foreground "orange"
                 :box (:line-width 1 :color "gold" :style released-button)
                 :overline "green"))))
    (elpher-search
     ((t
       (:inherit org-level-5
                 :slant oblique
                 :background "gray75"
                 :overline "green"
                 :underline (:color "cyan" :style wave)
                 :box (:line-width 3 :color "gold" :style pressed-button)))))
    :init ;; (@> "Sixel")
    (when (eq pegnumax-first-run 't)
      (shell-command (concat "cd " elisp-dir "; git clone git://thelambdalab.xyz/sixel.git && mv sixel/sixel.el .; rm -rf sixel"))
      (defvar sixel-provided
        (shell-command-to-string
         (concat "[[ -z \$\(grep -o sixel <\(grep \".provide 'sixel.\" < " elisp-dir "sixel.el\)\) ]] && echo -n \"\" || echo -n sixel ")))
      (when (equal sixel-provided "")
        (shell-command (concat "echo \"(provide 'sixel)\" >> " elisp-dir "sixel.el"))
        (delete-file (concat elisp-dir "sixel.elc"))))
    :config
    (use-package org)
    ;; (@* "Gemini")
    ;; FIXME - Problem with config
    ;; (use-package gemini-mode)
    ;; (use-package gemini-write :ensure nil) ;; Utilizes the Titan protocol; Learn more: (@url :file-name "https://alexschroeder.ch/cgit/gemini-write/about/" :display "Gemini & Titan mode")
    (use-package sixel :ensure nil :preface (declare-function sixel-render-images-in-buffer "sixel.el")))) ;; (@> "Sixel")

;; (@* "Emacs Web Wowser") - (@info :file-name "eww" :display "EWW")
(use-package eww
  :commands (eww eww-browse-url helm-eww helm-eww-bookmark)
  :config
  ;; Links numbering... modern web browsers /still/ don't do this!
  ;; `Nyxt' implements links numbering in a fashion that is similar to `pdf-links-action-perform'.
  (use-package  eww-lnum ;; `w3m' can also do this via `w3m-lnum' (bundled with said package).
    :bind (:map eww-mode-map
                ("f" . eww-lnum-follow)
                ("U" . eww-lnum-universal)))
  (use-package helm-eww
    :preface (declare-function helm-eww "helm-eww.el")
    :bind (([remap eww-list-histories] . helm-eww-history))))

;; (@* "WWW Miru (Pager)") - (@info :file-name "emacs-w3m" :display "Emacs-w3m User's Manual")
(use-package w3m
  ;; Ignore the official repository (i.e. "emacs-w3m/emacs-w3m").
  :ensure nil
  ;; Optional:
  ;; Build from (@url :file-name "https://salsa.debian.org/debian/w3m" :display "source"). Old versions contain vulnerabilities - see (@url :file-name "https://www.cvedetails.com/version-list/15995/35351/1/W3m-Project-W3M.html" :display "CVE Details >> W3m Project >> W3M: All Versions")
  ;; Makefile requires (@url :file-name "https://www.hboehm.info/gc/gc_source/gc6.4.tar.gz" :display "gc6.4") and `libgc-dev'.
  :ensure-system-package (w3m
                          (convert . imagemagick)
                          (java . openjdk)
                          patchelf
                          polipo
                          privoxy
                          i2pd
                          openssl
                          tor
                          (transmission-daemon . transmission)
                          tremc)
  :quelpa ((w3m :fetcher git :url "https://github.com/Boruch-Baum/emacs-w3m.git") :upgrade nil) ;; (@> "Boruch's Repo")
  :preface
  (declare-function w3m-polipo-tor-update-conf "init.el")
  (declare-function w3m-print-this-url "w3m.el" (INTERACTIVE-P))
  (declare-function w3m-delete-other-buffers "w3m.el" (BUFFER))
  (declare-function w3m-history-scrub "w3m-hist.el")
  (declare-function w3m-reload-this-page "w3m.el" (ARG NO-POPUP))
  (declare-function w3m--message "w3m.el" (timeout face &rest args))
  :bind
  (:map w3m-mode-map
        ("i"       . w3m-info)
        ("d"       . w3m-download-correctly)
        ("N"       . w3m-view-next-page)
        ("W"       . wttrin)
        ("n"       . w3m-page-nav--duckduckgo-style-next)
        ("p"       . w3m-page-nav--duckduckgo-style-previous)
        ("M-p"     . w3m-view-video)
        ("C-u M-s" . w3m-session-select-toggle) ;; Toggle popup window position to below/beside main window
        ("C-u M-p" . w3m-download-video-at-point)
        ("C-t m"   . w3m-goto-custom-magnet)
        ("C-t M-i" . w3m-i2p)
        ("C-t M-f" . w3m-freenet)
        ("C-t M-t" . w3m-start-polipo-tor-inquiry)
        ("C-t c"   . w3m-tor-new-circuit)
        ("C-t i"   . w3m-tor-new-identity)
        ("C-t C-y" . w3m-adjust-youtube-channel)
        ("C-x C-s" . w3m-type-ahead))
  :commands (w3m-session-save w3m-session-select w3m-find-file w3m-print-current-url)
  :hook ((w3m-mode          . w3m-type-ahead-mode)
         (w3m-display       . w3m-auto-kill-page-process)
         (kill-buffer-hook  . w3m-kill-page-process)
         (kill-emacs        . w3m-i2p-quit)
         (kill-emacs        . w3m-freenet-quit)
         (kill-emacs        . w3m-polipo-tor-kill-process))
  :custom
  (w3m-profile-directory "~/.w3m")
  (w3m-command-arguments '("-header" "DNT: 1"))
  (w3m-cookie-accept-domains '(".fsf.org" ".gnu.org"))
  (w3m-cookie-reject-domains '("."))
  (w3m-default-display-inline-images t)
  (w3m-external-view-temp-directory (concat w3m-profile-directory "/tmp"))
  (w3m-follow-redirection 30)
  (w3m-gui-clipboard-commands nil) ;; Prevent the use of `xsel' and errors associated with it.
  (w3m-key-binding 'info) ;; Only works when customized (`setq' fails to apply it for some reason)
  (w3m-search-default-engine "duckduckgo")
  (w3m-search-engine-alist ;; For more advanced control outside of `w3m', see (@> "Search Engines").
   (quote
    (("duckduckgo" "https://html.duckduckgo.com/html?q=%s" utf-8)
     ("debian-pkg" "https://packages.debian.org/search?&searchon=names&suite=stable&section=all&arch=amd64&keywords=%s" nil)
     ("debian-bts" "https://bugs.debian.org/cgi-bin/pkgreport.cgi?archive=yes&pkg=%s" nil)
     ("dictionary" "https://www.dictionary.com/cgi-bin/dict.pl?term=%s&r=67" nil)
     ;; PeerTube, via (@url :file-name "https://sepiasearch.org/" :display "SepiaSearch") (via SearX), preferred
     ("peertube"   "https://searx.info/?q=!sep %s&categories=none&time_range=year&language=en-US" nil "")
     ;; Using (@url :file-name "No longer using YouTube or Invidious, since both require JavaScript - " :display "CloudTube")/Invidious for private youtube viewing.
     ;; (@url :file-name "https://freetubeapp.io/" :display "FreeTube") and (@url :file-name "https://github.com/ytorg/Yotter" :display "Yotter") are good alternatives, outside of Emacs.
     ;; More details for youtube access are here: (@url :file-name "https://github.com/iv-org/invidious/issues/1320#issuecomment-667595476" :display "iv-org/invidious: Project Future")
     ;; ("youtube"    "https://tube.cadence.moe/search?q=%s" nil "")
     ("youtube"    "https://invidious.snopyta.org/search?q=%s" nil "")
     ("quo"        "http://quosl6t6c64mnn7d.onion/results?q=%s" nil "")
     ;; Full-text articles - always available
     ("pubmed"     "https://www.ncbi.nlm.nih.gov/pmc/?term=%s" nil "")
     ;; For articles hidden behind a paywall, provide Sci-Hub the Digital Object Identifier (DOI) as an argument.
     ("sci-hub"    "https://sci-hub.st/%s" nil "")
     ("wikipedia"  "https://en.wikipedia.org/wiki/Special:Search?search=%s" nil))))
  :after celestial-mode-line
  :init
  (setq warning-suppress-types '((w3m)))
  (setq warning-suppress-log-types '((w3m) (w3m)))
  ;; (@> "Download Correctly") - `w3m-download' utilizes a function known as `make-mutex',
  ;; which is C code found in `thread.c'.  `thread.c' wasn't written until version 26.1
  ;; of GNU Emacs; therefore, use `wget' where possible.
  (when (version< emacs-version "26.1")
    (defvar w3m-dir
      (substring (shell-command-to-string (concat "ls " elpa-dir " | grep 'w3m-[0-9]*\.[0-9]*$'")) 0 -1))
    (if (file-exists-p (concat elpa-dir w3m-dir "/w3m.elc"))
        (delete-file (concat elpa-dir w3m-dir "/w3m.elc")))
    (shell-command (concat "sed -i '/^.require .w3m-download.$/ d' " elpa-dir w3m-dir "/w3m.el"))
    (byte-compile-file (concat elpa-dir w3m-dir "/w3m.el")))
  ;; (@> "BitTorrent Interface") - `transmission-remote-cli' has been superseded by `tremc'.
  ;; Enter <RET> on a (@> "magnet link") or online `.torrent' file to activate BitTorrent client.
  (defvar w3m-dir
    (substring (shell-command-to-string (concat "ls " elpa-dir " | grep 'w3m-[0-9]*\.[0-9]*$'")) 0 -1))
  (when (equal "transmission-remote-cli\n" (shell-command-to-string (concat "grep -m 1 -o 'transmission-remote-cli' " elpa-dir "w3m-*/w3m.el")))
    (if (file-exists-p (concat elpa-dir w3m-dir "/w3m.elc"))
        (delete-file (concat elpa-dir w3m-dir "/w3m.elc")))
    (shell-command (concat "sed -i 's,transmission-remote-cli,tremc,' " elpa-dir w3m-dir "/w3m.el"))
    (byte-compile-file (concat elpa-dir w3m-dir "/w3m.el")))
  :config
  ;; For the external file viewer
  (unless (file-directory-p w3m-external-view-temp-directory)
    (mkdir w3m-external-view-temp-directory))

  ;; Loading `w3m' modules
  (unless (eq pegnumax-first-run 't) (use-package w3m-type-ahead :ensure nil))
  (use-package w3m-bookmark :ensure nil) ;; Prevents error: "w3m-arrived-setup: Symbol's function definition is void: w3m-bookmark-iterator"
  (use-package w3m-session ;; Web browsing session management in `w3m'
    :ensure nil
    :config
    (defun w3m-session-select-toggle nil
        (interactive)
        (w3m-session-select nil 't nil))

    ;; Fix YouTube channel appearance on Invidious.
    (defun w3m-adjust-youtube-channel nil
      (interactive)
      (w3m-next-image)
      (w3m-zoom-out-image 100)
      (w3m-next-image)
      (w3m-zoom-out-image 700)
      (beginning-of-buffer)
      (next-line))

    ;; (@* "View Online Videos")
    ;; Snippet Source: (@url :file-name "https://francismurillo.github.io/2017-01-11-Using-vlc-With-w3m/" :display "Using vlc with w3m")
    (defvar w3m-page-process)
    (defcustom w3m-video-executable "mpv"
      "The executable that can run a network video stream."
      :type 'file
      :group 'w3m-video)
    (defcustom w3m-video-args
      (list
       ;; Profile located in (@file :file-name "~/.config/mpv/mpv.conf" :display "MPV Configuration")
       "--profile=big-cache"
       )
      "Extra arguments to run `w3m-video-executable'"
      :type 'choice
      :group 'w3m-video)
    (defun w3m-video (&optional url)
      "Get video URL from image-anchor or title-anchor (preferred)."
      (defvar w3m-supported-url nil "Is the URL supported?")
      (setq w3m-supported-url
            (substring
             (shell-command-to-string
              (concat "youtube-dl -g "
                      (or url (w3m-anchor) w3m-current-url)
                      " 2> /dev/null || echo")) 0 -1))
      (if (string-equal w3m-supported-url "")
          (progn
            (setq w3m-supported-url
                  (shell-command-to-string
                   ;; Get video URLs from websites unsupported by `youtube-dl'.
                   (concat "~/Code/w3m-view-other.sh "
                           (or url (w3m-anchor) w3m-current-url))))
            (if (string-equal w3m-supported-url "")
                nil
              (substring w3m-supported-url 0 -1)))
        (or url (w3m-anchor) w3m-current-url)))
    (defun w3m-view-video ()
      "View the video url with `w3m-video-executable'."
      (interactive)
      (defvar w3m-view-other-title nil "Title of the video.")
      (setq w3m-view-other-title
            (shell-command-to-string
             (concat
              "echo --title=\"$(~/Code/w3m-view-other-title.sh "
              (or (w3m-anchor) w3m-current-url)
              ")\"")))
      (unless (string-equal w3m-view-other-title "--title=\n")
        (setq w3m-video-args
              (append w3m-video-args
                      (list
                       (substring w3m-view-other-title 0 -1)))))
      (let ((url (w3m-url-valid (w3m-video))))
        (if (null url)
            (prog1
                (w3m--message nil 'w3m-error "No video at point")
              nil)
          (lexical-let ((video-process
                         (apply #'start-process
                                (append
                                 (list
                                  (format "%s-%s" "w3m" w3m-video-executable)
                                  nil
                                  w3m-video-executable)
                                 w3m-video-args
                                 (list url)))))
            (with-current-buffer (current-buffer)
              (w3m-kill-page-process)
              (setq-local w3m-page-process video-process)))))
      (setq w3m-video-args (list "--profile=big-cache")))
    (defun w3m-kill-page-process ()
      "Kill the `w3m-page-process' of the current buffer."
      (interactive)
      (with-current-buffer (current-buffer)
        (when (and (boundp 'w3m-page-process)
                 (process-live-p w3m-page-process))
          (kill-process w3m-page-process))
        (setq-local w3m-page-process nil)))
    (defun w3m-auto-kill-page-process (_url)
      "If the page has a process, quit it."
      (w3m-kill-page-process))
    (defun w3m-download-correctly nil ;; (@> "Download Correctly")
      "Choose between 2 downloading methods, rather than the default - `w3m-download'."
      (interactive)
      (if (version< emacs-version "26.1")
          (progn
            (define-key w3m-mode-map (kbd "d") nil)
            (error "Detected Emacs %s.  `w3m-download' requires `thread.c', which can be found in Emacs 26.1 and above!" emacs-version))
        (when (and (not (eq pegnumax-first-run 't)) (file-exists-p (concat elisp-dir "w3m-type-ahead.el")))
          (use-package w3m-wget :ensure nil
            ;; Bugfix for `w3m-wget'
            :init
            (defvar wget-dir
              (substring (shell-command-to-string (concat "ls " elpa-dir " | grep 'wget-[0-9]*\.[0-9]*$'")) 0 -1))
            (when (not (equal "" (shell-command-to-string (concat "grep w3m-anchor " elpa-dir "wget-*/w3m-wget.el"))))
              (if (file-exists-p (concat elpa-dir wget-dir "/w3m-wget.elc"))
                  (delete-file (concat elpa-dir wget-dir "/w3m-wget.elc")))
              (shell-command (concat "sed -i \"s,(w3m-anchor),(w3m-get-text-property-around 'w3m-href-anchor),\" " elpa-dir wget-dir "/w3m-wget.el"))
              (byte-compile-file (concat elpa-dir wget-dir "/w3m-wget.el"))))
          (cond
           ((equal current-prefix-arg nil) ;; No C-u - Use (@> "GNU Downloader") by default
            (setq w3m-wget-substitute-download-command t)
            (w3m-wget (w3m-print-this-url 't)))
           ((equal current-prefix-arg '(4)) ;; C-u - Fallback to native downloader
            (setq w3m-wget-substitute-download-command nil)
            (w3m-download-using-w3m (w3m-print-this-url 't)))))))

    ;; (@* "Download Videos")
    ;; Snippet Source: (@url :file-name "https://www.emacswiki.org/emacs/WThreeMYoutube" :display "WThreeMYoutube")
    (defun w3m-download-video (url)
      "Invoke `youtube-dl' in a sub-process to download a video."
      (interactive)
      (if (not (w3m-url-valid url))
          (w3m--message nil 'w3m-error "Invalid URL.")
        (when (string-match "\\(\\&list=[^&]+\\)" url)
          (setq url
                (concat
                 (substring-no-properties url nil (match-beginning 0))
                 (substring-no-properties url (match-end 0)))))
        (let* (proc
               (buf (generate-new-buffer "*w3m-download-video*"))
               (base (and (string-match "//\\([^/]+\\)/" url)
                        (substring-no-properties url
                                                 (match-beginning 1) (match-end 1))))
               (args (catch 'found-replacement
                       (dolist (elem w3m-download-video-alist "--")
                         (when (string-match (car elem) base)
                           (throw 'found-replacement (cdr elem)))))))
          (with-current-buffer buf
                                        ; How can I customize this buffer so incoming ~C-m~ will be
                                        ; dynamically converted to ~C-j~ in order to appear correctly
                                        ; as new-lines?
                                        ; (set-buffer-file-coding-system 'utf-8-dos)
                                        ; (set-buffer-file-coding-system 'utf-8-mac)
            (insert (current-time-string)
                    "\n  youtube-dl " args " " url "\n\n")
                                        ; NOTE: There does exist a function `w3m-process-do' to evaluate
                                        ;       lisp code asyncronously.
            (setq proc
                  (start-process "w3m-download-video" buf "youtube-dl" args  url)))
          (w3m--message nil t "Requesting download.")
          (set-process-sentinel proc
                                (lambda (proc event)
                                  (let ((buf (process-buffer proc)))
                                    (with-current-buffer buf (insert event))
                                    (cond
                                     ((string-match "^finished" event)
                                      (w3m--message nil t
                                                    "Download complete. Check buffer %s for details." buf))
                                     ((string-match "^open" event) t)
                                     (t (w3m--message nil 'w3m-error
                                                      "Download error. Check buffer %s for details." buf)))))))))
    (defun w3m-download-video-at-point nil
      "Invoke `youtube-dl' in a sub-process to download the video at point."
      (interactive)
      (let ((url (w3m-url-valid (w3m-video))))
        (if (not url)
            (w3m--message t 'w3m-error "No URL found at point.")
          (w3m-download-video url))))

    ;; Go to a custom BitTorrent (@> "magnet link")
    (defun w3m-goto-custom-magnet (magnet &rest _args)
      (interactive "smagnet:?xt=urn:btih: ")
      (w3m--goto-torrent-url (concat "magnet:?xt=urn:btih:" magnet)))

    ;; (@* "Freenet")
    (defun w3m-freenet-install nil
      (defvar freenet-signature
        (substring
         (shell-command-to-string
          (concat "cd ~/Code && "
                  "gpg --import keyring.gpg |& "
                  "gpg --verify new_installer_offline_1488.jar.sig |& "
                  "gawk '/Good signature/ { print $2,$3 }'")) 0 -1))
      (if (string-equal freenet-signature "Good signature")
          (progn
            ;; Ensure that GuixSD has a symbolic link for `chmod' - Not true by default.
            (if (eq (shell-command "ls /bin/chmod 2> /dev/null") 2)
                (shell-command
                 (concat "echo "(shell-quote-argument (read-passwd "Creating symbolic link for `chmod' - Password? "))" | "
                         "sudo -S ln -s $(whereis chmod | gawk '{ print $2 }') /bin")))
            ;; Create the freenet directory.
            (unless (file-directory-p "~/Freenet")
              (mkdir "~/Freenet"))
            ;; Install via the console.
            (cd "~/Freenet")
            (async-shell-command "java -jar ~/Code/new_installer_offline_1488.jar -console"))
        (error (concat "The Free Network Project did not provide a good signature for their installer.  "
                       "Download and install manually - instructions: "
                       "https://github.com/freenet/wiki/wiki/Installing-on-POSIX"))))
    (defun w3m-freenet nil
      "Command for connecting to Freenet (by the Free Network Project)."
      (interactive)
      ;; Has Freenet already been started?  If so, then quit.
      (if (string-equal (shell-command-to-string "netstat -lntup |& grep -o 8888 | tr -d '\\n'") "8888")
          (w3m-freenet-quit)
        (when (yes-or-no-p "Start Freenet for w3m? ")
          (w3m-i2p-quit)
          (w3m-polipo-tor-kill-process)
          (setq w3m-user-agent w3m-user-agent-tor)
          (if (file-directory-p "~/Freenet")
              (progn
                ;; Start Freenet
                (shell-command (concat (getenv "HOME") "/Freenet/run.sh start"))
                (remove-output-buffer)
                ;; Is the Freenet Proxy established? (If no, then it's not running - could be on GuixSD).
                (unless (string-equal (shell-command-to-string "netstat -lntup |& grep -o 8888 | tr -d '\\n'") "8888")
                  (cd "~/Freenet")
                  ;; (@> "Patching Binaries")
                  ;; The ELF Binary must be patched on GuixSD due to a different file system hierarchy (e.g. /gnu/store).
                  ;; `glibc' has the interpreter (dynamic linker) and shared libraries we need.
                  ;; Ensure to sort based on the version number of `glibc' and to select the latest version.
                  (unless (string-equal (shell-command-to-string "ls -1 /gnu/store/*-glibc-[0-9]???/lib/ld-linux-x86-64.so.2 | sort -t - -k 3 -g -r | sed -n '1 p'")
                                        (shell-command-to-string "readelf -l bin/wrapper-linux-x86-64 | sed -n '/interpreter/ s,\\[,,; s,\\],,p' | gawk -F': ' '{ print $2 }'"))
                    ;; Patch the binary: Set the interpeter.
                    (shell-command "patchelf --set-interpreter $(ls -1 /gnu/store/*-glibc-[0-9]???/lib/ld-linux-x86-64.so.2 | sort -t - -k 3 -g -r | sed -n '1 p') bin/wrapper-linux-x86-64")
                    ;; Patch the binary: Set the RPATH.
                    (shell-command "patchelf --set-rpath $(dirname $(ls -1 /gnu/store/*-glibc-[0-9]???/lib/. | sort -t - -k 3 -g -r | sed -n '1 p')) bin/wrapper-linux-x86-64")
                    ;; Try running Freenet, again.
                    (shell-command (concat (getenv "HOME") "/Freenet/run.sh start"))
                    (remove-output-buffer)))
                ;; (@> "Proxy")
                (defvar w3m-FProxy "http://localhost:8888/" "The interface provided for interacting with your Freenet node.")
                (defun w3m-check-freenet nil
                  (w3m-browse-url w3m-FProxy))
                ;; It takes about 3-4 seconds for `freenet' to establish a connection.
                (run-at-time "5.0 sec" nil 'w3m-check-freenet))
            (if (y-or-n-p "Freenet is not installed on your system.  Install it? ") (w3m-freenet-install))))))
    (defun w3m-freenet-quit nil
      ;; Ensure Freenet is not running.
      (setq w3m-user-agent w3m-user-agent-odd)
      (when (string-equal (shell-command-to-string "netstat -lntup |& grep -o 8888 | tr -d '\\n'") "8888")
        (shell-command (concat (getenv "HOME") "/Freenet/run.sh stop"))
        (remove-output-buffer)))

    ;; (@* "I2P")
    (defun w3m-i2p-install nil
      (unless (file-exists-p "~/.i2pd/i2pd.conf")
        (shell-command "mkdir ~/.i2pd && touch ~/.i2pd/i2pd.conf")
        (shell-command "echo -e 'log = true\nipv6 = true\n[httpproxy]\nport = 4444' \>\> ~/.i2pd/i2pd.conf"))
      ;; Configuring Privoxy for I2P access to eepsites
      (unless (eq (shell-command "grep 4444 ~/.config/privoxy/config") 0)
        (unless (file-directory-p "~/.config/privoxy") (make-directory "~/.config/privoxy"))
        (cd "~/.config/privoxy")
        (shell-command (concat "touch config && echo 'forward .i2p localhost:4444' >> config"))))
    (defun w3m-i2p nil
      "Command for connecting to I2P (the Invisible Internet Project).  Switch from Onion Routing -> Garlic Routing."
      (interactive)
      ;; Has I2P already been started?  If so, then quit.
      (if (string-equal (shell-command-to-string "netstat -lntup |& grep -o 4444 | tr -d '\\n'") "4444")
          (w3m-i2p-quit)
        (when (yes-or-no-p "Start I2P for w3m? ")
          (w3m-freenet-quit)
          (w3m-polipo-tor-kill-process)
          (setq w3m-user-agent w3m-user-agent-tor)
          (if (file-directory-p "~/.i2pd")
              (progn
                ;; (@> "Proxy")
                (defvar w3m-Privoxy-port 8118 "With this setup, we can browse both normal websites and `.i2p' pages.")
                (defvar w3m-Invisible "http://localhost:7070/" "The interface provided for interacting with your Garlic node.")
                (unless (string-equal (shell-command-to-string "netstat -lntup |& grep -o 8118 | tr -d '\\n'") "8118")
                  (shell-command "privoxy ~/.config/privoxy/config"))
                (unless (string-equal (shell-command-to-string "netstat -lntup |& grep -o 4444 | tr -d '\\n'") "4444")
                  (async-shell-command "i2pd" "*I2PD*"))
                (remove-output-buffer)
                ;; We are going to replace polipo's tor port with privoxy's port.
                (w3m-add-polipo-proxy-arguments w3m-Privoxy-port)
                (when (or (string-equal w3m-current-url nil)
                         (string-equal w3m-current-url "about:")
                         (string-equal w3m-current-url "about:blank"))
                  (defun w3m-check-i2p nil
                    (w3m-browse-url w3m-Invisible))
                  ;; It takes about 3-4 seconds for `i2pd' to establish a circuit.
                  (run-at-time "4.0 sec" nil 'w3m-check-i2p)))
            (if (y-or-n-p "I2P is not installed on your system.  Install it? ") (w3m-i2p-install))))))
    (defun w3m-i2p-quit nil
      ;; Ensure I2P is not running.
      (setq w3m-user-agent w3m-user-agent-odd)
      (when (string-equal (shell-command-to-string "netstat -lntup |& grep -o 8118 | tr -d '\\n'") "8118")
        (shell-command "kill -9 $(pgrep '[p]rivoxy')"))
      (when (string-equal (shell-command-to-string "netstat -lntup |& grep -o 4444 | tr -d '\\n'") "4444")
        (shell-command "kill -INT $(cat ~/.i2pd/i2pd.pid)")
        (remove-output-buffer)))

    ;; (@* "TOR") -- (@url :file-name "https://www.torproject.org/" :display "The Onion Router")
    ;; Allow `w3m', `polipo', and `tor' to establish anonymity (better than "private browsing") when browsing the Internet.
    ;; (@url :file-name "http://francismurillo.github.io/2017-02-01-Browsing-w3m-Anonymously-With-tor/" :display "Browsing w3m anonymously with tor")
    (when (and (executable-find "polipo")
             (executable-find "tor"))
      (defvar w3m-user-agent-odd w3m-user-agent
        "My original User Agent string, which makes me look unique/odd on a network.")
      (defvar w3m-user-agent-tor "Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0"
        "Matches TOR Browser's fingerprint.")
      (defcustom w3m-polipo-cache-dir (expand-file-name "polipo-cache" user-emacs-directory)
        "Polipo cache directory."
        :type 'directory
        :group 'w3m-anonymous)
      (defcustom w3m-tor-cache-dir (expand-file-name "tor-cache" user-emacs-directory)
        "Tor cache directory."
        :type 'directory
        :group 'w3m-anonymous)
      (make-directory w3m-polipo-cache-dir t)
      (make-directory w3m-tor-cache-dir t)
      (defcustom w3m-tor-port 18050
        "Tor port"
        :type 'number
        :group 'w3m-anonymous)
      (defcustom w3m-polipo-port 18123
        "Polipo port"
        :type 'number
        :group 'w3m-anonymous)
      (defcustom w3m-polipo-conf-file (expand-file-name "polipo-w3m-conf" user-emacs-directory)
        "Polipo configuration."
        :type 'file
        :group 'w3m-anonymous)
      (defcustom w3m-tor-conf-file (expand-file-name "tor-w3m-conf" user-emacs-directory)
        "Tor configuration."
        :type 'file
        :group 'w3m-anonymous)
      (defun w3m-polipo-tor-update-conf ()
        "Create/update `w3m-polipo-conf-file' and `w3m-tor-conf-file' with new configurations."
        (interactive)
        ;; (@file :file-name "~/.emacs.d/polipo-w3m-conf" :display "polipo-w3m-conf")
        (with-temp-file w3m-polipo-conf-file
          (insert
           (string-join
            (mapcar
             (lambda (pair)
               (pcase-let ((`(,key . ,value) pair))
                 (format
                  "%s = %s"
                  key
                  (cl-typecase value
                    (symbolp (symbol-name value))
                    (numberp (number-to-string value))
                    (stringp (format "\"%s\"" value))))))
             `(("proxyAddress" . "127.0.0.1")
               ("allowedClients" . "127.0.0.1")
               ("diskCacheRoot" . ,w3m-polipo-cache-dir)
               ("proxyPort" . ,w3m-polipo-port)
               ("cacheIsShared" . false)
               ("socksParentProxy" .
                ,(format "%s:%s" "localhost" (number-to-string w3m-tor-port)))
               ("socksProxyType" . socks5)))
            "\n")))
        ;; (@file :file-name "~/.emacs.d/tor-w3m-conf" :display "tor-w3m-conf")
        (with-temp-file w3m-tor-conf-file
          (insert
           (string-join
            (mapcar
             (lambda (pair)
               (pcase-let ((`(,key . ,value) pair))
                 (format
                  "%s %s"
                  key
                  (cl-typecase value
                    (symbolp (symbol-name value))
                    (numberp (number-to-string value))
                    (stringp value)))))
             `(("SocksPort" . ,w3m-tor-port)
               ("DataDirectory" . ,w3m-tor-cache-dir)
               ("ControlPort" . ,(1+ w3m-tor-port))
               ("DisableDebuggerAttachment" . 0)))
            "\n"))))
      (defvar w3m-polipo-process nil
        "Polipo process.")
      (defvar w3m-tor-process nil
        "Tor process.")
      (defun w3m-remove-proxy-arguments ()
        "Remove proxy arguments from `w3m-command-arguments'"
        (setq w3m-command-arguments
              (cl-reduce
               (lambda (val xs)
                 (if (and (string= "-o" val)
                        (or (string-prefix-p "http_proxy=" (or (car xs) ""))
                           (string-prefix-p "https_proxy=" (or (car xs) ""))))
                     (cdr xs)
                   (cons val xs)))
               w3m-command-arguments
               :from-end t
               :initial-value (list))))

      ;; (@* "Proxy")
      (defun w3m-add-polipo-proxy-arguments (polipo-port)
        "Add `polipo-port' to `w3m-command-arguments'"
        (setq w3m-command-arguments
              (append w3m-command-arguments
                      (list "-o"
                            (format
                             "http_proxy=http://127.0.0.1:%s/"
                             (number-to-string polipo-port)))
                      (list "-o"
                            (format
                             "https_proxy=https://127.0.0.1:%s/"
                             (number-to-string polipo-port))))))

      ;; TOR port (18050) Established? (@L "(message (shell-command-to-string \"netstat -lntup |& grep 18050 | tr -d '\\n'\"))")
      (defun w3m-polipo-tor-start-process (&rest _args)
        "Start `w3m-polipo-process'."
        (interactive)
        (w3m-i2p-quit)
        (w3m-freenet-quit)
        (setq w3m-user-agent w3m-user-agent-tor)
        (unless (process-live-p w3m-tor-process)
          (message "Starting tor process.")
          (w3m-polipo-tor-update-conf)
          (setq w3m-tor-process
                (start-process "w3m-tor" "*w3m-tor*" "tor" "-f" w3m-tor-conf-file)))
        (unless (process-live-p w3m-polipo-process)
          (message "Starting polipo process.")
          (setq w3m-polipo-process
                (start-process "w3m-polipo" "*w3m-polipo*" "polipo" "-c" w3m-polipo-conf-file)))
        (w3m-remove-proxy-arguments)
        (w3m-add-polipo-proxy-arguments w3m-polipo-port))
      (defun w3m-polipo-tor-kill-process (&rest _args)
        "Kill `w3m-polipo-process'."
        (interactive)
        (setq w3m-user-agent w3m-user-agent-odd)
        (when (process-live-p w3m-polipo-process)
          (message "Killing polipo process")
          (kill-process w3m-polipo-process))
        (when (process-live-p w3m-tor-process)
          (message "Killing tor process")
          (kill-process w3m-tor-process))
        (setq w3m-polipo-process nil
              w3m-tor-process nil)
        (w3m-remove-proxy-arguments))
      (defun w3m-tor-new-circuit nil ;; Inspired by TOR Project - (@url :file-name "https://support.torproject.org/glossary/new-tor-circuit-for-this-site/" :display "New Tor Circuit for this Site")
        "Execute if a tor exit relay is unable to connect to a specific website."
        (interactive)
        (w3m-polipo-tor-kill-process)
        ;; `w3m-polipo-tor-start-process' always creates a new IP Address.
        (w3m-polipo-tor-start-process))
      (defun w3m-tor-new-identity nil ;; Inspired by TOR Project - (@url :file-name "https://support.torproject.org/glossary/new-identity/" :display "New Identity")
        "Prevent your subsequent browser activity from being linked to what you were doing before."
        (interactive)
        (w3m-browse-url "about:")
        (w3m-delete-other-buffers buffer-file-name)
        (w3m-history-scrub) ;; Mapped to ~C-DEL~
        ;; No need to delete cookies.  They are properly managed, and are only used for trusted clearnet websites.
        (w3m-goto-url-new-session "https://check.torproject.org/")
        (w3m-tor-new-circuit)
        (w3m-reload-this-page nil nil))
      (add-to-list 'w3m-no-proxy-domains "127.0.0.1")
      (add-to-list 'w3m-no-proxy-domains "localhost")
      (defun w3m-start-polipo-tor-inquiry nil
        (interactive)
        (if (process-live-p w3m-tor-process)
            (w3m-polipo-tor-kill-process)
          (when (yes-or-no-p "Start polipo and tor for w3m? ")
            (w3m-polipo-tor-start-process)
            (when (or (string-equal w3m-current-url nil)
                     (string-equal w3m-current-url "about:")
                     (string-equal w3m-current-url "about:blank"))
              (defun w3m-check-tor nil
                (w3m-browse-url "https://check.torproject.org/"))
              ;; It takes about 3-4 seconds for `tor' to establish a circuit.
              (run-at-time "4.0 sec" nil 'w3m-check-tor))))))))

;; (@* "Search Engines")
;; Minor mode for defining and querying search engines
(use-package engine-mode
  :commands (engine-mode
             engine/execute-search
             engine/get-query
             engine/search-duckduckgo
             engine/search-emacswiki
             engine/search-github
             engine/search-google-scholar
             engine/search-internet-archive
             engine/search-peertube
             engine/search-project-gutenberg
             engine/search-pubmed-central
             engine/search-quo
             engine/search-sci-hub
             engine/search-stack-overflow
             engine/search-wikipedia
             engine/search-wiktionary
             engine/search-youtube)
  :custom
  (engine/browser-function 'w3m-browse-url)
  :init
  (engine-mode t)
  :config ;; Search Engine Definitions
  (defengine duckduckgo       "https://html.duckduckgo.com/html?q=%s"                                                :keybinding "d")
  (defengine duckduckgo-onion "https://3g2upl4pq6kufc4m.onion/html?q=%s"                                             :keybinding "M-d")
  (defengine emacswiki        "https://html.duckduckgo.com/html?q=site\:emacswiki.org+%s+site\:emacswiki.org&ia=web" :keybinding "e")
  (defengine github           "https://github.com/search?ref=simplesearch&q=%s"                                      :keybinding "M-g")
  (defengine internet-archive "https://archive.org/search.php?query=%s"                                              :keybinding "a")
  ;; PeerTube, via (@url :file-name "https://sepiasearch.org/" :display "SepiaSearch") (via SearX), preferred
  (defengine peertube          "https://searx.info/?q=!sep %s&categories=none&time_range=year&language=en-US"        :keybinding "v")
  ;; Using (@url :file-name "No longer using YouTube or Invidious, since both require JavaScript - " :display "CloudTube")/Invidious for private youtube viewing.
  ;; (@url :file-name "https://freetubeapp.io/" :display "FreeTube") and (@url :file-name "https://github.com/ytorg/Yotter" :display "Yotter") are good alternatives, outside of Emacs.
  ;; More details for youtube access are here: (@url :file-name "https://github.com/iv-org/invidious/issues/1320#issuecomment-667595476" :display "iv-org/invidious: Project Future")
  ;; (defengine youtube           "https://tube.cadence.moe/search?q=%s"                                                :keybinding "y")
  (defengine youtube           "https://invidious.snopyta.org/search?q=%s"                                           :keybinding "y")
  (defengine quo               "http://quosl6t6c64mnn7d.onion/results?q=%s"                                          :keybinding "q")
  (defengine project-gutenberg "https://www.gutenberg.org/ebooks/search/?query=%s"                                   :keybinding "g")
  ;; Full-text articles - always available
  (defengine pubmed-central    "https://www.ncbi.nlm.nih.gov/pmc/?term=%s"                                           :keybinding "p")
  ;; For articles hidden behind a paywall, provide Sci-Hub the Digital Object Identifier (DOI) as an argument.
  (defengine sci-hub           "https://sci-hub.st/%s"                                                               :keybinding "s")
  (defengine google-scholar    "https://scholar.google.com/scholar?hl=en&q=%s"                                       :keybinding "M-s")
  (defengine stack-overflow    "https://stackoverflow.com/search?q=%s"                                               :keybinding "o")
  (defengine wikipedia         "https://www.wikipedia.org/search-redirect.php?language=en&go=Go&search=%s"           :keybinding "w")
  (defengine wiktionary        "https://www.wikipedia.org/search-redirect.php?family=wiktionary&language=en&go=Go&search=%s"
    :keybinding "C-w"))

;; (@* "Email")
(use-package rmail :disabled)
(unless (package-installed-p 'wanderlust) (fset 'eval-when-compile 'progn)) ;; Dirty hack for `wl-e21.el'
(use-package wl
  :ensure wanderlust
  :ensure-system-package (pinentry (gpg . gnupg) (gnutls-cli . gnutls))
  :preface
  (defvar eof-block-branches nil)
  (defvar eof-block-reg nil)
  ;; (@* "MIME") - Learn more about (@info :file-name "emacs-mime" :node "Top" :display "Multipurpose Internet Mail Extensions")
  (declare-function mime-edit-insert-file "mime-edit.el" (FILE) (&optional VERBOSE))
  (declare-function mime-entity-content-type "mime.el" (ENTITY))
  (declare-function mime-save-content "mime-play.el" (ENTITY SITUATION))
  (declare-function wl-message-decrypt-pgp-nonmime "wl-mime.el")
  (declare-function wl-summary-toggle-disp-msg "wl-summary.el" (&optional ARG))
  (declare-function wl-summary-toggle-disp-msg "wl-summary.el" (&optional ARG))
  (defun djcb-popup (title msg &optional icon sound)
    "Show a popup if we're on X, or echo it otherwise; TITLE is the title
of the message, MSG is the context. Optionally, you can provide an ICON and
a sound to be played."
    (interactive)
    (when sound (shell-command (concat "mpv --really-quiet " sound " 2> /dev/null")))
    (if (eq window-system 'x)
        (when (executable-find "notify-send")
          (shell-command (concat "notify-send " (if icon (concat "-i " icon) "") " '" title "' '" msg "'")))
      ;; text only version
      (message (concat title ": " msg)))
    (remove-output-buffer))
  ;; Snippet source (@url :file-name "https://emacs-fu.blogspot.com/2009/09/wanderlust-tips-and-tricks.html#sec-1.6" :display "Check outgoing mail ")
  (defun djcb-wl-draft-subject-check ()
    "check whether the message has a subject before sending"
    (if (and (< (length (std11-field-body "Subject")) 1)
             (null (y-or-n-p "No subject! Send current draft?")))
        (error "Abort.")))
  (defun djcb-wl-draft-attachment-check ()
    "if attachment is mention but none included, warn the the user"
    (save-excursion
      (goto-char 0)
      (unless ;; Do we have an attachment?
          (re-search-forward "^Content-Disposition: attachment" nil t)
        (when ;; No attachments.  Did we mention an attachment?
            (re-search-forward "attach" nil t)
          (unless (y-or-n-p "Possibly missing an attachment. Send current draft?")
            (error "Abort."))))))
  ;; Snippet source: (@url :file-name "https://psg.com/lists/wanderlust/msg01164.html" :display "Re: Adding multiple attachments")
  (defun mime-edit-insert-multiple-files ()
    "Insert MIME parts from multiple files."
    (interactive)
    (catch 'done
      (while t
        (let ((next-file (expand-file-name
                          (read-file-name "Insert file as MIME message: "))))
          (if (and (file-exists-p next-file)
                   (file-regular-p next-file))
              (with-no-warnings (mime-edit-insert-file next-file t))
            (throw 'done nil)))
        (if (not (y-or-n-p "Insert another? "))
            (throw 'done nil)))))
  (defun mime-preview-extract-current-entity (&optional ignore-examples)
    "Extract current entity into file (maybe).
It decodes current entity to call internal or external method as
\"extract\" mode.  The method is selected from variable `mime-acting-condition'."
    (interactive "P")
    (cl-letf (((symbol-function #'mime-play-entity)
               (lambda (entity &optional situation _ignored-method)
                 (mime-save-content entity situation))))
      (mime-preview-play-current-entity ignore-examples "extract")))
  (defun ck-message-decrypt-pgp-nonmime()
    (interactive)
    (if (and wl-message-buffer (get-buffer-window wl-message-buffer))
        (let ((start) (ends))
          (wl-summary-toggle-disp-msg 'on)
          (with-current-buffer
              (goto-char (point-min))
            (re-search-forward "^-----BEGIN PGP MESSAGE-----$")
            (setq start (point))
            (goto-char (point-min))
            (re-search-forward "^-----END PGP MESSAGE-----$")
            (setq ends (point))
            (goto-char start)
            (set-mark-command nil)
            (goto-char ends)
            (wl-message-decrypt-pgp-nonmime)))
      (message "no message to decrypt")))
  :commands (compose-mail wl wl-draft)
  :hook ((wl-draft-mode        . (lambda nil (beacon-mode -1) (setq fill-column 72)))
         (mime-view-mode       . (lambda nil (visual-line-mode t)))
         (mime-edit-mode       . (lambda nil (auto-fill-mode) (emojify-mode) (mime-edit-set-sign nil) (mime-edit-set-encrypt nil)))
         (wl-message-redisplay . (lambda nil (smiley-region (point-min) (point-max))))
         (wl-mail-send-pre     . djcb-wl-draft-subject-check)
         (wl-mail-send-pre     . djcb-wl-draft-attachment-check) ;; Attach file: ~C-c C-x TAB~
         (wl-exit              . (lambda nil (beacon-mode +1) (setq fill-column 158))))
  :bind (:map wl-summary-mode-map
              ("C-c d" . ck-message-decrypt-pgp-nonmime) ("C-c C-x C-a" . mime-edit-insert-multiple-files)
              ("M-[" . w3m-zoom-out-image)
              ("M-]" . w3m-zoom-in-image))
  :init
  (setq wl-folders-file (concat user-emacs-directory ".folders"))
  (add-hook 'wl-biff-notify-hook ;; Notify me when new mail is received.
            (lambda()
              (djcb-popup "Wanderlust" "You have new mail!"
                          ;; "/usr/share/icons/gnome/32x32/status/mail-unread.png"
                          (substring (shell-command-to-string "ls /gnu/store/*mate-[0-9]*/share/icons/hicolor/32x32/categories/instant-messaging.png") 0 -1)
                          ;; "/usr/share/sounds/ubuntu/stereo/phone-incoming-call.ogg")))
                          (substring (shell-command-to-string "ls /gnu/store/*sound-theme*/share/sounds/freedesktop/stereo/phone-incoming-call.oga | sed -n '1 p'") 0 -1))))
  (autoload 'wl-user-agent-compose "wl-draft" nil t)
  :config
  ;;  (@info :file-name "emacs-w3m" :node "SEMI MUAs" :display "Reading HTML mails in SEMI MUAs")
  (use-package mime-w3m :ensure nil :demand t) ;; Make HTML emails human-readable with (@> "WWW Miru (Pager)")
  ;; Prevents an `mu' bug: (void-function defun-maybe-cond)
  (use-package pym :ensure nil :demand t)
  (use-package mu-cite ;; Cite the way I like.
    :hook (mail-citation . mu-cite-original)
    :init
    (setq mu-cite-top-format '("On " date ", " from " spake thus:\n"))
    (setq mu-cite-prefix-format (quote ("> ")))) ;; Type ~g~ in folder or summary buffers.
  (use-package format-flowed
    :ensure nil
    :hook (mime-display-text/plain-hook . (lambda ()
                                            (defvar format-flowed-entity)
                                            (when (string= "flowed"
                                                           (cdr (assoc "format"
                                                                       (mime-content-type-parameters
                                                                        (mime-entity-content-type format-flowed-entity)))))
                                              (fill-flowed))))
    :bind (:map wl-draft-mode-map ("C-c F" . format-flowed-translate-message))
    :init
    (autoload 'fill-flowed "flow-fill")
    (with-eval-after-load "MIME-Edit" ;; Support quoted text.
      (add-hook 'mime-edit-mode-hook 'messages-are-flowing-use-and-mark-hard-newlines)))
  (use-package elmo-search ;; Allow a method to query email messages.
    :ensure nil
    :ensure-system-package (mu)
    :commands elmo-search-register-engine
    :init
    (elmo-search-register-engine
     'mu   'local-file
     :prog (substring (shell-command-to-string "which mu") 0 -1)
     :args '("find" pattern "--fields" "l") :charset 'utf-8)
    (setq elmo-search-default-engine 'mu)
    (setq wl-default-spec "["))
  ;; (@* "BBDB")
  (use-package bbdb
    :hook ((mail-setup       . bbdb-insinuate-message)
           (bbdb-notice-mail . bbdb-auto-notes))
    :bind (:map wl-draft-mode-map (("<tab>" . bbdb-complete-mail)))
    :commands bbdb-initialize
    :init
    (bbdb-initialize 'wl 'message)
    (bbdb-mua-auto-update-init 'message)
    (use-package bbdb-wl
      :ensure nil
      :commands bbdb-insinuate-wl
      :init    (bbdb-insinuate-wl))
    (use-package bbdb-message
      :ensure nil
      :commands bbdb-insinuate-message
      :init    (bbdb-insinuate-message))
    (setq bbdb-wl-folder-regexp "^INBOX$\\|^Sent"))
  ;; (@* "SMTP") - See (@url :file-name "https://www.fsf.org/resources/webmail-systems" :display "Free Software Webmail Systems") for other options.
  (setq wl-from                      "Michael Pagan <michael.pagan@member.fsf.org>"
        wl-smtp-posting-user         "michael.pagan@mailbox.org"
        wl-smtp-posting-server       "smtp.mailbox.org"
        wl-smtp-authenticate-type    "login"
        wl-smtp-connection-type      'ssl
        wl-smtp-posting-port          465
        wl-local-domain              "mailbox.org"
        wl-message-id-domain         "smtp.mailbox.org"
        wl-fcc                       "+archive/sent1"
        wl-fcc-force-as-read          t
        wl-draft-folder              "+archive/draft1"
        wl-draft-preview-process-pgp  t
        wl-trash-folder              "mailbox+Trash"
        mime-edit-pgp-signers        '("27CDF56D")
        mml-secure-openpgp-signers   '("27CDF56D")
        mml-secure-openpgp-sign-with-sender t
        mml-secure-openpgp-encrypt-to-self  t)
  ;; (@* "IMAP") - See my (@> "IMAP Default Folder").
  (setq imap-ssl-program                     'tls-program
        smtp-use-gnutls                      'tls-program
        starttls-use-gnutls                   t
        elmo-imap4-default-stream-type       'ssl
        elmo-imap4-default-authenticate-type 'login)
  (setq wl-stay-folder-window        t
        wl-folder-window-width       30
        wl-folder-desktop-name      "Messages"
        wl-default-spec             ".***/***"
        wl-interactive-save-folders nil)
  (setq wl-auto-select-next                  'unread
        wl-summary-width                      nil
        wl-summary-weekday-name-lang         "en"
        wl-summary-showto-folder-regexp      "^Sent.*"
        wl-summary-line-format               "%n%T%P %M/%D (%W) %h:%m %t%[%25(%c %f%) %] %s"
        wl-summary-always-sticky-folder-list  t
        wl-thread-insert-opened               t
        wl-thread-open-reading-thread         t)
  (setq wl-user-mail-address-list
        (quote ("michael.pagan@member.fsf.org" "michael.pagan@mailbox.org")))
  (setq wl-forward-subject-prefix "Fwd: ")

  ;; Invert behaviour of with and without argument replies.
  (setq wl-draft-reply-without-argument-list
        '(("Reply-To" ("Reply-To") nil nil)
          ("Mail-Reply-To" ("Mail-Reply-To") nil nil)
          ("From" ("From") nil nil)))
  (setq wl-draft-reply-with-argument-list
        '(("Followup-To" nil nil ("Followup-To"))
          ("Mail-Followup-To" ("Mail-Followup-To") nil ("Newsgroups"))
          ("Reply-To" ("Reply-To") ("To" "Cc" "From") ("Newsgroups"))
          ("From" ("From") ("To" "Cc") ("Newsgroups"))))

  (setq wl-draft-config-alist
        '(((string-match "member.fsf.org" wl-draft-parent-folder)
           (template . "PUBLIC"))
          ;; automatic for replies
          (reply "\\(To\\|Cc\\|Delivered-To\\): .*member.fsf.org.*"
                 (template . "PUBLIC"))
          ))
  ;; Choose template with ~C-c C-j~.
  (setq wl-template-alist
        '(("PUBLIC"
           (wl-from                   . "Michael Pagan <michael.pagan@member.fsf.org>")
           ;; (@> "IMAP Default Folder") - Learn more about (@info :file-name "wl" :node "IMAP Folder" :display "IMAP setup under Wanderlust")
           (wl-default-folder         . "%INBOX:\"michael.pagan@mailbox.org\"/login@imap.mailbox.org:993")
           (wl-draft-folder           . "+archive/draft1")
           (wl-trash-folder           . "mailbox+Trash")
           (wl-smtp-posting-user      . "michael.pagan@mailbox.org")
           (wl-smtp-posting-server    . "smtp.mailbox.org")
           (wl-smtp-authenticate-type . "login")
           (wl-smtp-connection-type   . 'ssl)
           (wl-smtp-posting-port      .  465)
           (wl-local-domain           . "mailbox.org")
           (wl-message-id-domain      . "smtp.mailbox.org")
           ("From"                    .  wl-from)
           ("Fcc"                     . "+archive/sent1")
           (lambda nil ;; Sign messages by default.
             (setq mime-edit-pgp-signers      '("27CDF56D")
                   mml-secure-openpgp-signers '("27CDF56D")
                   mml-secure-openpgp-sign-with-sender t
                   mml-secure-openpgp-encrypt-to-self  t)))
          ;; <Add more templates here>
          ))
  ;; Auto add signature on draft edit.
  (remove-hook 'wl-draft-send-hook 'wl-draft-config-exec)
  (add-hook    'wl-mail-setup-hook 'wl-draft-config-exec)
  (setq wl-draft-config-alist
        '(((string-match "1" "1")
           (bottom . "\n-- \n") (bottom-file . "~/.signature"))))

  ;; Sending: Don't split large messages
  (setq mime-edit-split-message nil)
  (setq wl-dispose-folder-alist
        ;; (@> "IMAP Default Folder") - Learn more about (@info :file-name "wl" :node "IMAP Folder" :display "IMAP setup under Wanderlust")
        '(("^%.*mailbox\\.org" . "%Trash:\"michael.pagan@mailbox.org\"/login@imap.mailbox.org:993")
          ;; <Add more Trash bins here>
          ))
  ;; (@* "BIFF") - Mail notifications
  (setq wl-biff-check-folder-list
        ;; (@> "IMAP Default Folder") - Learn more about (@info :file-name "wl" :node "IMAP Folder" :display "IMAP setup under Wanderlust")
        '("%INBOX:\"michael.pagan@mailbox.org\"/login@imap.mailbox.org:993"
          ;; <Add more Inboxes here>
          "rss:http://proveallthings.weebly.com/1/feed"))
  (setq wl-biff-check-interval 1800) ;; Check every 30 minutes for new mail
  (setq wl-biff-use-idle-timer t)
  (setq wl-strict-diff-folders wl-biff-check-folder-list)

  ;; Set mail-icon to be shown universally in the modeline.
  (setq global-mode-string
        (cons
         '(wl-modeline-biff-status
           wl-modeline-biff-state-on
           wl-modeline-biff-state-off)
         global-mode-string))
  (if (boundp 'mail-user-agent)
      (setq mail-user-agent 'wl-user-agent))
  (if (fboundp 'define-mail-user-agent)
      (define-mail-user-agent
        'wl-user-agent
        'wl-user-agent-compose
        'wl-draft-send
        'wl-draft-kill
        'mail-send-hook)))

;; (@* "XMPP")
(use-package jabber ;; Use (@man :page "finch" :display "Finch"), instead (packaged with `pidgin')
  :defer t
  ;; Packages listed after `pidgin' represent dependencies for the `lurch' ((@> "OMEMO")) plugin.
  ;; `mxml' (Mini-XML) is also required, but must be built and configured on GuixSD.
  :ensure-system-package (pidgin
                          cmake
                          pkg-config
                          (gcc . gcc-toolchain)
                          (glib-compile-schemas . glib)
                          (libgcrypt-config . libgcrypt)
                          (xml2-config . libxml2)
                          xz)
  :bind* (("C-x C-j C-f" . jabber-finch)
          ("C-x C-j C-c" . jabber-connect)
          ("C-x C-j C-d" . jabber-disconnect))
  :init
  ;; (@* "ELIM - Emacs Lisp Instant Messenger (Alpha)")
  ;; ELIM/Garak supports OTR; however, it does not support (@> "OMEMO"), which supercedes it.
  (autoload 'garak "garak" nil t) ;; GUI for the ELIM
  ;; Truncate long chat buffers and enable icons.
  (setq lui-max-buffer-size 30000
        tree-widget-image-enable t)
  ;; (@* "Finch") - Terminal jabber client
  (defun jabber-finch nil
    (interactive)
    (ansi-term "finch" "jabber-finch")
    (emojify-mode))
  ;; (@> "Patching Binaries") - `lurch.so' needs to read the shared library from (@> "Mini-XML").
  (unless (eq pegnumax-first-run 't)
    (cd "~/.purple/plugins")
    (when (eq (shell-command "readelf -d lurch.so | grep '/usr/local/lib'") 1)
      (shell-command "patchelf --set-rpath $(readelf -d lurch.so | sed -n '/runpath/ s,.*\\[,,p' | sed -n 's,\],:/usr/local/lib,p') lurch.so"))
    (cd user-emacs-directory))
  (when (eq pegnumax-first-run 't)
    ;; Install (@> "ELIM")
    (defun jabber-install-elim nil
      (unless (file-directory-p (concat elisp-dir "elim"))
        (cd elisp-dir)
        (shell-command "git clone git://git.savannah.gnu.org/elim.git")
        (cd (concat elisp-dir "elim"))
        (compile "make")
        (other-window 1)
        (end-of-buffer)
        (rename-buffer "*ELIM-Compilation*")
        (other-window 1)
        (end-of-buffer)))
    (jabber-install-elim)
    ;; Install (@> "Mini-XML").
    (unless (file-exists-p "/usr/local/lib/libmxml.so.1")
      (cd "~/Code")
      (shell-command "tar -zxvf mxml-3.2.tar.gz && cd mxml-3.2; ./configure && make")
      (shell-command
       (concat "cd ~/Code/mxml-3.2; echo "(shell-quote-argument (read-passwd "Install `mxml' - Password? "))" | "
               "sudo -S make install")))
    ;; (@* "OMEMO") - (@> "Ensure C Compiler is found") before execution
    ;; Install `lurch'.
    (defun jabber-install-lurch nil
      (unless (file-exists-p "~/.purple/plugins/lurch.so")
        (cd "~/Code")
        (shell-command "git clone https://github.com/gkdr/lurch/")
        (cd "~/Code/lurch")
        (shell-command "git submodule update --init --recursive")
        (compile "make install-home")
        (other-window 1)
        (end-of-buffer)
        (rename-buffer "*Lurch-Compilation*")
        (other-window 1)
        (end-of-buffer)))
    (run-at-time "30.0 sec" nil 'jabber-install-lurch)
    ;; Install `carbons'.
    (unless (file-exists-p "~/.purple/plugins/carbons.so")
      (cd "~/Code")
      (shell-command "git clone https://github.com/gkdr/carbons.git")
      (cd "~/Code/carbons")
      (shell-command "make")
      (shell-command "make install-home"))
    (if (get-buffer  "*Shell Command Output*")
        (kill-buffer "*Shell Command Output*"))))

;; (@* "Post-Package Management Settings")
;; Ensure the remaining packages defined in `package-selected-packages' are installed.
(when (eq pegnumax-first-run 't)
  (setq compilation-scroll-output t)
  (switch-to-buffer "*Compile-Log*")
  (end-of-buffer))
(setq use-dialog-box nil)
(package-install-selected-packages)
(add-to-list 'package-selected-packages 'w3m)
(add-to-list 'package-selected-packages 'wanderlust)
(unless (version< emacs-version "25.3")
  (add-to-list 'package-selected-packages 'dashboard)
  (add-to-list 'package-selected-packages 'ytel))
(unless (version< emacs-version "26.1") (add-to-list 'package-selected-packages 'phi-grep))
(unless (version< emacs-version "26.2") (add-to-list 'package-selected-packages 'elpher))
(unless (version< emacs-version "26.3") (add-to-list 'package-selected-packages 'keypression))
(when (version< emacs-version "27.0") (add-to-list 'package-selected-packages 'fill-column-indicator))
(package-autoremove)
(when (eq pegnumax-first-run 't)
  (byte-recompile-directory elisp-dir 0)
  (switch-to-buffer "*Compile-Log*")
  (end-of-buffer)
  (delete-other-windows))

;; (@* "Ergonomic Navigation Commands")
;; My Macros
(fset 'vim-scroll-up "\C-u1\M-v")
(fset 'vim-scroll-down "\C-u1\C-v")
(fset 'page-up-on-line-feed "\C-x[\C-l\C-l")
(fset 'page-down-on-line-feed "\C-x]\C-l\C-l")
(fset 'bongo-set-mpv-backend
      (kmacro-lambda-form [?\C-x ?\C-m ?b ?o ?- ?s ?- ?b ?a ?c ?k ?e tab return ?m ?p ?v return] 0 "%d"))

;; My Global key bindings
(global-set-key (kbd "C-x w") 'choose-browser)
(global-set-key (kbd "M-+") 'e2wm:start-management)
(global-set-key (kbd "C-M-+") 'e2wm:stop-management)
;; [Menu] button on my keyboard is bound to `execute-extended-command', or ~M-x~ (is it built into Emacs?).
(global-set-key "\C-x\C-m" 'execute-extended-command)    ;; `Meta-X' is now streamlined!
(global-set-key "\C-c\C-m" 'execute-extended-command)    ;; Forgive sloppiness for `Meta-X'.
(global-set-key "\C-w" 'backward-kill-word)              ;; Better than `Backspace'
(global-set-key "\C-x\C-k" 'kill-region)                 ;; Easier way to yank text
(global-set-key [f5] 'start-kbd-macro)                   ;; Start recording your keystrokes as a macro (define with [f4])
(global-set-key [f7] 'spray-mode)                        ;; For (@> "Speed Reading")
(global-set-key (kbd "<prior>") 'vim-scroll-up)          ;; Can't say you miss it now ...
(global-set-key (kbd "<next>") 'vim-scroll-down)         ;; Ditto ...
(global-set-key (kbd "C-c }") 'page-down-on-line-feed)   ;; Faster scrolling within a file containing line feeds (^L)
(global-set-key (kbd "C-c {") 'page-up-on-line-feed)     ;; Ditto
(global-set-key (kbd "C-x k") 'kill-this-buffer)         ;; No need to be asked which buffer to kill when in said buffer
(global-set-key (kbd "C-c k") 'close-and-kill-next-pane) ;; Kill the `other-window'.
(global-set-key (kbd "C-x 4 k") 'kill-matching-buffers)  ;; Offer to kill all buffers matching a regular expression.
(global-set-key (kbd "C-x 8 k") 'kill-some-buffers)      ;; Offer to kill each buffer, one by one.
(global-set-key (kbd "C-c C-x C-c") 'kill-emacs)         ;; Really kill Emacs (the daemon).
(global-set-key [f12] 'my-scratch-buffer)                ;; Welcome to `PeGnuMax'!
(global-set-key (kbd "C-c M") 'bongo-set-mpv-backend)
(global-set-key (kbd "C-c M-0") 'image-transform-reset)
(global-set-key (kbd "C-c M-m") 'image-transform-fit-to-height)
(global-set-key (kbd "C-c C-n") 'highlight-changes-next-change)
(global-set-key (kbd "C-c C-p") 'highlight-changes-previous-change)
(use-package which-key
  :demand t
  :commands (which-key-mode which-key-setup-minibuffer)
  :custom
  (which-key-popup-type 'minibuffer)
  (which-key-echo-keystrokes 0.25)
  (which-key-idle-delay 0.4)
  :config
  (which-key-setup-minibuffer)
  (which-key-mode))

(unless (version< emacs-version "26.3")
  (use-package keypression
    :hook     (keypression-mode . (lambda nil (dimmer-mode 'toggle)))
    :after celestial-mode-line
    :custom
    (keypression-y-offset 30) ;; Keep the display above the modeline
    (keypression-cast-command-name-format "%s  %s")
    (keypression-combine-same-keystrokes t)
    (keypression-fade-out-delay 1.0)
    (keypression-frame-justify 'keypression-left-justified)
    (keypression-cast-command-name t)
    (keypression-font-face-attribute '(:width normal :height 200 :weight bold))))

;; Take a look at (@url :file-name "https://github.com/dakra/dmacs/blob/master/init.org#helpful-a-better-help-buffer" :display "Helpful: A better help buffer").
(use-package helpful ;; Better than the standard Emacs `help' utility
  :defines (-zip-pair -second-item -drop-last)
  :bind* (("C-h f"   . helpful-callable)
          ("C-h v"   . helpful-variable)
          ("C-h k"   . helpful-key)
          ("C-c C-." . helpful-at-point))
  :config
  (use-package elisp-demos
    :commands  elisp-demos-advice-helpful-update
    :init
    (advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)))
(use-package bookmark+
  :ensure nil
  :commands edit-bookmarks ;; ~C-x pe~  | ~C-x rl~ (set bookmarks: ~C-x rm~)
  :custom
  (bmkp-bmenu-state-file nil)
  (bmkp-last-as-first-bookmark-file nil))
(unless (version< emacs-version "26.1")
  (use-package frog-jump-buffer
    :bind ("C-c M-j" . frog-jump-buffer)
    :config
    (dolist (regexp '("TAGS" "^\\*Compile-log" "-debug\\*$" "^\\:" "errors\\*$" "^\\*Backtrace" "-ls\\*$"
                      "stderr\\*$" "^\\*Flymake" "^\\*vc" "^\\*Warnings" "^\\*eldoc" "\\^*Shell Command"))
      (push regexp frog-jump-buffer-ignore-buffers))))

;; (@* "Emacs Aesthetics (Advanced UI)")
(use-package egg-timer)
(use-package whitespace :ensure nil :delight :defer t)
(use-package wc-mode :defer 0.1 :bind* ("C-c w" . wc-mode))
(use-package simple
  :ensure    delight
  :commands
  (auto-fill-function
   set-mark-command
   shell-command
   shell-command-to-string
   visual-line-mode)
  :ensure nil
  :delight  (auto-fill-function " AF") (visual-line-mode))
(use-package beacon
  :demand    t
  :unless   (get-buffer "*WL-Demo*")
  :commands  beacon-mode
  :custom   (beacon-color "#666600")
  :bind*    ("C-c *" . beacon-mode)
  :after     mode-icons
  :config   (beacon-mode))
(use-package on-screen
  :demand    t
  :commands  on-screen-global-mode
  :after     beacon
  :config   (on-screen-global-mode +1))
(use-package window-number
  :demand    t
  :commands  window-number-mode
  ;; `window-number-string' freezes (i.e. `redisplay_internal`) `ediff' frame, hence this binding.
  :bind*    ("C-x C-j -" . window-number-mode)
  :after     on-screen
  :config   (window-number-mode 1))
(use-package page-break-lines
  :demand    t
  :commands  global-page-break-lines-mode
  :after     window-number
  :config   (global-page-break-lines-mode))
(use-package typo ;; Punctuaction mark cycling (e.g. dashes, quotation marks, and dots)
  :hook ((org-mode  . typo-mode)
         (text-mode . auto-fill-mode)
         (text-mode . ruler-mode)
         (text-mode . toggle-word-wrap)))
(use-package filladapt
  ;; :disabled ;; prevents me from executing `report-emacs-bug'
  :hook ((text-mode org-mode) . filladapt-mode))
(use-package volatile-highlights
  :preface (declare-function volatile-highlights-mode "volatile-highlights.el" (&optional ARG))
  :delight
  :demand t
  :after moe-theme-switcher
  :config (volatile-highlights-mode t))
(use-package pretty-mode
  :hook (prog-mode . (lambda nil (if (string-equal window-system "x") (turn-on-pretty-mode)))))
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode)
  :custom-face (rainbow-delimiters-unmatched-face ((nil :foreground "red" :inherit 'error :box t))))
(use-package easy-escape ;; Nicer elisp regex syntax highlighting
  :delight (easy-escape-minor-mode " Ez^")
  :hook ((emacs-lisp-mode lisp-mode) . easy-escape-minor-mode))
(use-package zoom-frm
  :ensure nil
  :unless noninteractive
  :bind (:map ctl-x-map
              ([(control ?+)] . zoom-in/out)
              ([(control ?-)] . zoom-in/out)
              ([(control ?=)] . zoom-in/out)
              ([(control ?0)] . zoom-in/out)))
(use-package dimmer
  :unless noninteractive
  :preface (declare-function dimmer-mode "dimmer.el" (&optional ARG))
  :after solar
  :custom
  (dimmer-fraction 0.25)
  (dimmer-mode t))

;; (@* "Modeline")
(use-package solar
  :ensure nil
  :config
  (use-package calendar
    :ensure nil
    :custom
    ;; (@> "Greeneville")
    (calendar-latitude       36.1632)
    (calendar-longitude     -82.8310)
    (calendar-location-name "Greeneville, Tennessee"))
  ;; (@* "Date")
  (use-package time
    :ensure nil
    :custom
    (display-time-24hr-format t)
    (display-time-day-and-date t)
    (display-time-default-load-average nil)
    (display-time-format "%a %b %d %H:%M | "))
  ;; (@* "Weather")
  (use-package wttrin
    :defer t
    :init
    ;; Workaround: (@url :file-name "https://github.com/bcbcarl/emacs-wttrin/issues/16#issuecomment-658987903" :display "Raw html, instead of wttrin #16")
    (defvar wttrin-dir
      (substring (shell-command-to-string (concat "ls " elpa-dir " | grep 'wttrin-'")) 0 -1))
    (when (string-equal (shell-command-to-string (concat "grep -o User-Agent " elpa-dir wttrin-dir "/wttrin.el")) "User-Agent\n")
      (if (file-exists-p (concat elpa-dir wttrin-dir "/wttrin.elc"))
          (delete-file (concat elpa-dir wttrin-dir "/wttrin.elc")))
      (shell-command (concat "cd " elpa-dir wttrin-dir "; "
                             "sed -i 's,let ((url-.*)),let ((url-user-agent \"curl\")),' wttrin.el && "
                             "sed -i 's,(concat \"http:.*),(concat \"http://wttr.in/\" query \"?A\"),' wttrin.el"))
      (byte-compile-file (concat elpa-dir wttrin-dir "/wttrin.el")))
    (setq wttrin-default-cities          '("Greeneville")
          wttrin-default-accept-language '("Accept-Language" . "en-US")))
  ;; (use-package sunshine
  ;;   ;; Bug Report: (@url :file-name "https://github.com/aaronbieber/sunshine.el/issues/19" :display "Wrong dates reported within `sunshine-forecast-date-face' #19")
  ;;   :commands (sunshine-forecast)
  ;;   :custom
  ;;   (sunshine-show-icons 't)
  ;;   (sunshine-location "Greeneville")
  ;;   :custom-face
  ;;   (sunshine-forecast-date-face ((t (:foreground "cyan" :weight ultra-bold))))
  ;;   :config
  ;;   (if (file-exists-p (concat user-emacs-directory "sunshine-api-key.el"))
  ;;       (load (locate-user-emacs-file "sunshine-api-key.el"))))
  (use-package celestial-mode-line
    :after moe-theme-switcher
    :custom (celestial-mode-line-suffix " | ")
    :preface
    (declare-function celestial-mode-line-start-timer "celestial-mode-line.el")
    ;; The default icons are:
    (defvar celestial-mode-line-phase-representation-alist '((0 . "○") (1 . "☽") (2 . "●") (3 . "☾")))
    (defvar celestial-mode-line-sunrise-sunset-alist '((sunrise . "☀↑") (sunset . "☀↓")))
    ;; You can get text-only icons as follows:
    (defvar celestial-mode-line-phase-representation-alist '((0 . "( )") (1 . "|)") (2 . "(o)") (3 . "|)")))
    (defvar celestial-mode-line-sunrise-sunset-alist '((sunrise . "*^") (sunset . "*v")))
    :config
    ;; (@> "Boonton")
    ;; (setq calendar-latitude 40.9026
    ;;      calendar-longitude -74.4071
    ;;      calendar-location-name "Boonton, New Jersey")
    ;; Setting up time display in the modeline
    (setq global-mode-string '("" celestial-mode-line-string display-time-string fancy-battery-mode-line "  |"))
    (celestial-mode-line-start-timer)
    ;; (@* "Battery")
    (use-package fancy-battery ;; Displayed on the far right of the modeline
      :preface
      (declare-function display-time-mode "time.el.gz" (&optional ARG))
      (declare-function fancy-battery-mode "fancy-battery.el" (&optional ARG))
      :after celestial-mode-line
      :custom
      (fancy-battery-show-percentage t)
      :config
      (display-time-mode)
      (fancy-battery-mode))))

;; (@* "Mode Icons")
(use-package mode-icons
  :preface
  (defvar grep-string
    (shell-command-to-string (concat "grep f1dd " elpa-dir "mode-icons-*/mode-icons.el")))
  :unless    noninteractive
  :commands  mode-icons-mode
  :demand    t
  :init
  ;; (@* "Emojis")
  (use-package emojify :if window-system :hook (org-mode . emojify-mode))

  ;; Bugfix for conflict between `mode-icons' and `all-the-icons-dired'
  ;; Replace `fa-file-o' (\f016) with `fa-paragraph' (\f1dd).
  (when (equal "" grep-string)
    (defvar mode-icons-dir
      (substring (shell-command-to-string (concat "ls " elpa-dir " | grep 'mode-icons-[0-9]*\.[0-9]*$'")) 0 -1))
    (if (file-exists-p (concat elpa-dir mode-icons-dir "/mode-icons.elc"))
        (delete-file (concat elpa-dir mode-icons-dir "/mode-icons.elc")))
    (shell-command (concat "sed -i 's/#xf016/#xf1dd/' " elpa-dir mode-icons-dir "/mode-icons.el"))
    (byte-compile-file (concat elpa-dir mode-icons-dir "/mode-icons.el")))
  :after celestial-mode-line
  ;; Adding new associations for different mode types.
  :config
  (mode-icons-mode 1)
  (setq mode-icons (append '(("\\`Calc\\'"                       #xf1ec FontAwesome)) mode-icons)
        mode-icons (append '(("\\`Change Log\\'"                 #xf044 FontAwesome)) mode-icons)
        mode-icons (append '(("\\`Dashboard\\'"                  #xf00b FontAwesome)) mode-icons)
        mode-icons (append '(("\\`Debb.*\\'"                     #xf188 FontAwesome)) mode-icons)
        mode-icons (append '(("\\` ?all-the-icons-dired-mode\\'" #xf0e8 FontAwesome)) mode-icons)
        mode-icons (append '(("\\` ?Dired-du\\'"                 #xf0a0 FontAwesome)) mode-icons)
        mode-icons (append '(("\\`Disk Usage\\'"                 #xf0a0 FontAwesome)) mode-icons)
        mode-icons (append '(("\\`elpher\\'"                     #xf1e6 FontAwesome)) mode-icons)
        mode-icons (append '(("\\`enwc\\'"                       #xf1eb FontAwesome)) mode-icons)
        mode-icons (append '(("\\`EPUB\\'"                       #xf02d FontAwesome)) mode-icons)
        mode-icons (append '(("\\`eww\\'"                        #xf19c FontAwesome)) mode-icons)
        mode-icons (append '(("\\`Helpful\\'"                    #xf059 FontAwesome)) mode-icons)
        mode-icons (append '(("\\`HTML[+]\\'"                    "html"         xpm)) mode-icons)
        mode-icons (append '(("\\`image-dired-image-display\\'"  #xf1c5 FontAwesome)) mode-icons)
        mode-icons (append '(("\\`image-dired-thumbnail\\'"      #xf03e FontAwesome)) mode-icons)
        mode-icons (append '(("\\` ?Isearch\\'"                  #xf002 FontAwesome)) mode-icons)
        mode-icons (append '(("\\`jabber-roster\\'"              #xf27b FontAwesome)) mode-icons)
        ;; No GNU icon, yet: (@url :file-name "https://github.com/FortAwesome/Font-Awesome/issues/2665" :display "https://github.com/FortAwesome/Font-Awesome/issues/2665")
        ;; Still no GNU icon: (@url :file-name "https://github.com/ForkAwesome/Fork-Awesome/issues/145" :display "https://github.com/ForkAwesome/Fork-Awesome/issues/145")
        ;; mode-icons (append '(("\\`Makefile\\'"                   #xf6e3 FontAwesome)) mode-icons)
        ;; mode-icons (append '(("\\`GNUmakefile\\'"                #xf6e3 FontAwesome)) mode-icons)
        mode-icons (append '(("\\` ?Linkd\\'"                    ":link:"     emoji)) mode-icons)
        mode-icons (append '(("\\`Message\\'"                    ":email:"    emoji)) mode-icons)
        mode-icons (append '(("\\`Outline\\'"                    #xf0ca FontAwesome)) mode-icons)
        mode-icons (append '(("\\`Package Menu\\'"               #xf187 FontAwesome)) mode-icons)
        mode-icons (append '(("\\`PDFView\\'"                    #xf1c1 FontAwesome)) mode-icons)
        mode-icons (append '(("\\`Shell\\'"                      "bash"      xpm-bw)) mode-icons)
        mode-icons (append '(("\\`Snake\\'"                      #xf11b FontAwesome)) mode-icons)
        mode-icons (append '(("\\`Tetris\\'"                     #xf12e FontAwesome)) mode-icons)
        mode-icons (append '(("\\`TeX\\'"                        "tex"          ext)) mode-icons)
        mode-icons (append '(("\\` ?TmpB\\'"                     ":ghost:"    emoji)) mode-icons)
        mode-icons (append '(("\\`w3m\\'"                        #xf19c FontAwesome)) mode-icons)
        mode-icons (append '(("\\` ?WK\\'"                       #xf11c FontAwesome)) mode-icons)
        mode-icons (append '(("\\`Weather Forecast Mode\\'"      #xf0e9 FontAwesome)) mode-icons)))

;; (@* "Completion & Narrowing Framework")
(unless (eq pegnumax-first-run 't)
  (use-package helm
    :diminish
    :hook (helm-major-mode . turn-on-tempbuf-mode)
    :bind (([remap execute-extended-command] . helm-M-x)
           ("C-x C-f" . helm-find-files)
           ("C-x b"   . helm-mini)     ;; See buffers & recent files; more useful.
           ("C-x r b" . helm-filtered-bookmarks)
           ("C-x C-r" . helm-recentf)  ;; Search for recently edited files
           ("C-c i"   . helm-imenu)
           ("C-h a"   . helm-apropos)
           ;; Look at what was cut recently & paste it in.
           ("M-y"     . helm-show-kill-ring)

           :map helm-map
           ("C-w"     . backward-kill-word)
           ;; We can list actions on the currently selected item, with ~C-z~.
           ("C-z"     . helm-select-action)
           ;; Let's keep tab-completetion anyhow.
           ("TAB"     . helm-execute-persistent-action)
           ("<tab>"   . helm-execute-persistent-action))
    :after linkd
    :config (helm-mode t)))

;; (@* "Dashboard")
(unless (version< emacs-version "25.3")
  (unless (eq pegnumax-first-run 't)
    (use-package dashboard
      :defer t
      :diminish (dashboard-mode page-break-lines-mode)
      :bind
      (("C-c d" . open-dashboard)
       :map dashboard-mode-map
       (("n" . dashboard-next-line)
        ("p" . dashboard-previous-line)
        ("N" . dashboard-next-section)
        ("P" . dashboard-previous-section)))
      :custom
      (dashboard-startup-banner 1)
      (dashboard-items '((recents   . 7)
                         (bookmarks . 7)
                         (agenda    . 7)))
      (dashboard-set-heading-icons t)
      (dashboard-set-file-icons t)
      (dashboard-set-navigator t)
      (dashboard-navigator-buttons
       (if (featurep 'all-the-icons)
           `(((,(all-the-icons-faicon "gitlab" :height 1.1 :v-adjust -0.05)
               "PegGnuMax" "Browse PeGnuMax Homepage"
               (lambda (&rest _) (browse-url "https://gitlab.com/pegzmasta/dot-emacs")))
              (,(all-the-icons-fileicon "elisp" :height 1.0 :v-adjust -0.1)
               "Configuration" "" (lambda (&rest _) (find-file "~/.emacs.d/init.el")))
              (,(all-the-icons-faicon "cogs" :height 1.0 :v-adjust -0.1)
               "Update" "" (lambda (&rest _) (auto-package-update-now)))))
         `((("" "PeGnuMax" "Browse PeGnuMax Homepage"
             (lambda (&rest _) (browse-url "https://gitlab.com/pegzmasta/dot-emacs")))
            ("" "Configuration" "" (lambda (&rest _) (find-file "~/.emacs.d/init.el")))
            ("" "Update" "" (lambda (&rest _) (auto-package-update-now)))))))
      :init
      ;; (@url :file-name "https://qiita.com/Ladicle/items/feb5f9dce9adf89652cf#%E6%B0%97%E5%88%86%E3%81%8C%E9%AB%98%E3%81%BE%E3%82%8B%E8%B5%B7%E5%8B%95%E7%94%BB%E9%9D%A2----emacs-dashbaord" :display "気分が高まる起動画面 -- emacs-dashbaord")
      (defvar dashboard-dir
        (substring (shell-command-to-string (concat "ls " elpa-dir " | grep 'dashboard-'")) 0 -1))
      (cd user-emacs-directory)
      (if (not (string-equal (shell-command-to-string (concat "diff 1.txt " elpa-dir dashboard-dir "/banners/1.txt")) ""))
          (shell-command (concat "cp 1.txt " elpa-dir dashboard-dir "/banners")))
      ;; Otherwise, the icons won't appear
      :after
      (all-the-icons)
      :config
      (dashboard-modify-heading-icons '((recents   . "file-text")
                                        (bookmarks . "book")))
      (dashboard-setup-startup-hook)

      ;; Open Dashboard function.
      (defun open-dashboard ()
        "Open the *dashboard* buffer and jump to the first widget."
        (interactive)
        (if (get-buffer dashboard-buffer-name)
            (kill-buffer dashboard-buffer-name))
        (dashboard-insert-startupify-lists)
        (switch-to-buffer dashboard-buffer-name)
        (goto-char (point-min))
        (delete-other-windows)))))

;; (@* "Games")
(use-package chess
  :ensure-system-package (gnuchess . chess)
  :defer t
  :config
  (setq chess-images-separate-frame       nil ;; Do *not* open in a new frame.
        chess-display-highlight-legal     t
        chess-display-highlight-last-move t
        chess-images-directory  (concat user-emacs-directory "chess-images/")))
(use-package gnugo :ensure-system-package gnugo :defer t)

;; (@* "Editor Customizations")
(use-package eev :commands eev-beginner) ;; (@> "GNU Eev")
(use-package dired-du ;; Display the disk usage of directories in human readable format in `dired'.
  :defer t
  :custom
  (dired-du-size-format t)
  (dired-du-update-headers t)
  :config
  (use-package disk-usage :ensure nil :defer t)) ;; Provided by GuixSD via package `emacs-disk-usage'
(use-package dired ;; (@info :file-name "dired-x" :display "Dired Extra Features.")
  :ensure nil
  :hook ((dired-mode . hl-line-mode)
         (dired-mode . dired-du-mode)
         ;; FIXME: Latest change (2021-06-14) in `all-the-icons-dired' prevents this line from working
         (dired-mode . dired-omit-mode)
         (dired-mode . turn-on-tempbuf-mode)
         (dired-mode . all-the-icons-dired-mode))
  :preface
  (defun reicon-dired-mode nil
    "Disable `all-the-icons-dired', so that TRAMP can work without hanging; reactivate, if already disabled."
    (interactive)
    (helm-mode 'toggle)
    (if (eq all-the-icons-dired-mode t)
        (remove-hook 'dired-mode-hook 'all-the-icons-dired-mode)
      (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)))
  (defun dired-toggle-read-only-and-icons nil ;; Emacs 27 Bug: Can't commit changes in `wdired' with icons
    "De-activate/activate `all-the-icons-dired-mode' before entering into `wdired'."
    (interactive)
    (if (eq all-the-icons-dired-mode t)
        (all-the-icons-dired-mode -1)
      (all-the-icons-dired-mode +1))
    (dired-toggle-read-only))
  :commands dired-toggle-read-only
  :custom (dired-listing-switches "-valsh") ;; TRAMP can't handle `--group-directories-first'.
  :config
  (use-package dired-x
    :ensure nil
    :custom
    (dired-omit-files "^\\.\\|~$\\|^#\\|^-"))
  (use-package dired+
    :ensure nil
    :preface (declare-function diredp-toggle-find-file-reuse-dir "dired+.el" (FORCE-P))
    :init (setq diredp-hide-details-initially-flag nil)
    :after celestial-mode-line
    :config (diredp-toggle-find-file-reuse-dir t))
  (use-package sudo-edit :defer t :config (use-package tramp-theme :unless noninteractive) :after moe-theme-switcher)
  (bind-keys
   :map dired-mode-map
   ("C-x C-q" . dired-toggle-read-only-and-icons)
   ("C-x M-d" . reicon-dired-mode)
   ("C-c C-r" . sudo-edit))
  :bind*
  ([remap electric-newline-and-maybe-indent] . dired-jump))
(use-package wdired
  :ensure nil
  :defer t
  :preface
  (defadvice wdired-exit (after restore-icons nil activate)
    (all-the-icons-dired-mode +1))
  (defun wdired-finish-edit-restore-icons nil
    "Activate `all-the-icons-dired-mode' when commiting a change in `wdired'."
    (interactive)
    (wdired-finish-edit)
    (all-the-icons-dired-mode +1))
  (defun wdired-abort-changes-restore-icons nil
    "Activate `all-the-icons-dired-mode' when aborting changes in `wdired'."
    (interactive)
    (wdired-abort-changes)
    (all-the-icons-dired-mode +1))
  :commands (wdired-finish-edit wdired-abort-changes)
  :custom   (wdired-allow-to-change-permissions t) ;; Make permission bits editable.
  :config
  (bind-keys ;; Workaround for bug in `wdired' whilst `all-the-icons-dired-mode' is on
   :map wdired-mode-map
   ("C-c C-c" . wdired-finish-edit-restore-icons)
   ("C-c C-k" . wdired-abort-changes-restore-icons)))

;; (@* "Image Processing")
(use-package image-dired
  :unless noninteractive
  :after celestial-mode-line
  :custom (image-dired-track-movement nil)
  :config
  (use-package image+ ;; For zooming in/out of an image
    :commands  imagex-global-sticky-mode
    :config   (imagex-global-sticky-mode 1)))
(use-package uimage ;; Hooks for viewing online images in buffers
  :unless noninteractive
  :hook ((info-mode wiki-mode) . uimage-mode))

;; (@* "Text Editing Modes")
(add-to-list 'auto-mode-alist '("\\.\\(?:a\\|elf\\|so\\)\\'" . elf-mode))
(autoload 'visual-basic-mode "visual-basic-mode" "Visual Basic mode." t)
(push '("\\.\\(?:frm\\|\\(?:ba\\|cl\\|vb\\)s\\)\\'" . visual-basic-mode) auto-mode-alist)
(use-package csv-mode :defer t :config (setq csv-separators '("," ":" ";" "    " "\t")))
(use-package rtf-mode :defer t :ensure nil :after shell)
(use-package pcmpl-args :defer t :after shell) ;; Argument completion in `shell-mode' and `eshell'
(use-package sed-mode :after shell)
(use-package wgrep :defer t :after shell)
(if (not (version< emacs-version "26.1")) (use-package phi-grep :after shell :bind* ("<f6>" . phi-grep-in-directory)))
(use-package lua-mode
  :preface (defun lua-outline-mode () (setq-local outline-regexp "function"))
  :mode "\\.lua\\'"
  :interpreter ("lua" . lua-mode)
  :hook (lua-mode . lua-outline-mode)
  :bind (:map lua-mode-map ("M-?" . lua-search-documentation) ("M-." . dumb-jump-go))
  :config
  (setq lua-documentation-function 'eww)
  (use-package lua-block
    :ensure nil
    :commands lua-block-mode
    :config  (lua-block-mode t)))
(use-package markdown-mode
  :init (setq markdown-command "multimarkdown")
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :config
  (unless (version< emacs-version "26.2")
    (bind-keys :map markdown-mode-map ("C-c m" . elpher-redraw)))
  (setq markdown-display-remote-images t)
  (setq markdown-fontify-code-blocks-natively t)
  (add-to-list 'markdown-code-lang-modes '("ini" . conf-mode)))

;; Ensure Org aesthetics are applied (required as of 2020 September 16).
;; `custom-face' fails without this function.  This occurred after (@url :file-name "https://github.com/kuanyui/moe-theme.el/issues/106" :display "this bug"), which led me to (@url :file-name "https://github.com/kuanyui/moe-theme.el/issues/107" :display "file this report").
(defun make-org-ready nil
  (interactive)
  ;; (@url :file-name "http://doc.norang.ca/org-mode.html" :display "Org Mode - Organize Your Life In Plain Text!")
  (use-package org ;; (@info :file-name "org" :display "Org Mode")
    :defer t
    ;; :ensure org-plus-contrib
    :preface (defvar org-mime-library 'mml)
    :commands org-mode
    :hook (org-capture-before-finalize-hook . add-property-with-date-captured)
    :bind (("C-c a" . org-agenda) ("C-c c" . org-capture) ("C-c l" . org-store-link))
    :custom
    (org-deadline-warning-days              7)
    (org-directory          "~/Desktop/notes")
    (org-enforce-todo-checkbox-dependencies t)
    (org-log-done                           t)
    (org-use-fast-todo-selection            t)
    (org-log-repeat                    "time")
    (org-log-into-drawer                    t)
    (org-todo-log-states                    nil)
    (org-pretty-entities                    t)
    (org-lowest-priority                    ?C)
    (org-highest-priority                   ?A)
    (org-default-priority                   ?A)
    (org-enforce-todo-dependencies          t)
    (org-image-actual-width (/ (display-pixel-width) 3))
    (org-todo-keywords
     (quote ((sequence "TODO(t)" "NEXT(n!)" "PEND(p!)" "|" "DONE(d!)" "DEFER(h!)" "SCRUB(s!)"))))
    :custom-face
    (org-document-title  ((t (:foreground "#afd7ff" :weight bold :height 2.75))))
    (org-level-1         ((t (:foreground "#5fafd7" :weight bold :height 2.50))))
    (org-level-2         ((t (:foreground "#a1db00" :weight bold :height 2.25))))
    (org-level-3         ((t (:foreground "#ff8700" :weight bold :height 2.00))))
    (org-level-4         ((t (:foreground "#00d7af" :weight bold :height 1.75))))
    (org-level-5         ((t (:foreground "#ef2929" :weight bold :height 1.50))))
    (org-level-6         ((t (:foreground "#af5fff" :weight bold :height 1.25))))
    (org-special-keyword ((t (:foreground "#d75f00" :slant oblique))))
    :init
    ;; If Org's contrib directory does not exist or is missing (@file :file-name "~/.emacs.d/elpa/contrib/scripts/ditaa.jar" :display "ditaa.jar"), then make sure to install it.
    (unless (file-exists-p (concat elpa-dir "contrib/scripts/ditaa.jar"))
      (shell-command
       (concat
        "mkdir -p " elpa-dir "contrib/scripts && "
        "wget https://github.com/jwiegley/org-mode/raw/master/contrib/scripts/ditaa.jar -O "
        elpa-dir "contrib/scripts/ditaa.jar")))
    :after
    (moe-theme-switcher)
    :config
    ;; Snippet source: (@url :file-name "https://github.com/rougier/svg-tag-mode/blob/main/examples/example-2.el" :display "SVG Tag Mode - Examples")
    (use-package svg-tag-mode
      :disabled
      :config
      (defconst date-re "[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}")
      (defconst time-re "[0-9]\\{2\\}:[0-9]\\{2\\}")
      (defconst day-re "[A-Za-z]\\{3\\}")
      (defun svg-progress-percent (value)
        (svg-image (svg-lib-concat
                    (svg-lib-progress-bar (/ (string-to-number value) 100.0)
                                          nil :margin 0 :stroke 2 :radius 3 :padding 2 :width 11)
                    (svg-lib-tag (concat value "%")
                                 nil :stroke 0 :margin 0)) :ascent 'center))

      (defun svg-progress-count (value)
        (let* ((seq (mapcar #'string-to-number (split-string value "/")))
               (count (float (car seq)))
               (total (float (cadr seq))))
          (svg-image (svg-lib-concat
                      (svg-lib-progress-bar (/ count total) nil
                                            :margin 0 :stroke 2 :radius 3 :padding 2 :width 11)
                      (svg-lib-tag value nil
                                   :stroke 0 :margin 0)) :ascent 'center)))

      (setq svg-tag-tags
            `(
              ;; Org tags
              (":\\([A-Za-z0-9]+\\)" . ((lambda (tag) (svg-tag-make tag))))
              (":\\([A-Za-z0-9]+[ \-]\\)" . ((lambda (tag) tag)))

              ;; Task priority
              ("\\[#[A-Z]\\]" . ( (lambda (tag)
                                  (svg-tag-make tag :face 'org-priority
                                                :beg 2 :end -1 :margin 0))))

              ;; Progress
              ("\\(\\[[0-9]\\{1,3\\}%\\]\\)" . ((lambda (tag)
                                          (svg-progress-percent (substring tag 1 -2)))))
              ("\\(\\[[0-9]+/[0-9]+\\]\\)" . ((lambda (tag)
                                          (svg-progress-count (substring tag 1 -1)))))

              ;; TODO / DONE
              ("TODO" . ((lambda (tag) (svg-tag-make "TODO" :face 'org-todo :inverse t :margin 0))))
              ("NEXT" . ((lambda (tag) (svg-tag-make "NEXT" :face 'org-todo :margin 0))))
              ("PEND" . ((lambda (tag) (svg-tag-make "PEND" :face 'org-todo :margin 0))))
              ("DONE" . ((lambda (tag) (svg-tag-make "DONE" :face 'org-done :inverse t :margin 0))))
              ("DEFER" . ((lambda (tag) (svg-tag-make "DEFER" :face 'org-done :margin 0))))
              ("SCRUB" . ((lambda (tag) (svg-tag-make "SCRUB" :face 'org-done :inverse t :margin 0))))

              ;; Citation of the form [cite:@Knuth:1984]
              ("\\(\\[cite:@[A-Za-z]+:\\)" . ((lambda (tag)
                                           (svg-tag-make tag
                                                         :inverse t
                                                         :beg 7 :end -1
                                                         :crop-right t))))
              ("\\[cite:@[A-Za-z]+:\\([0-9]+\\]\\)" . ((lambda (tag)
                                                   (svg-tag-make tag
                                                                 :end -1
                                                                 :crop-left t))))


              ;; Active date (without day name, with or without time)
              (,(format "\\(<%s>\\)" date-re) .
               ((lambda (tag)
                  (svg-tag-make tag :beg 1 :end -1 :margin 0))))
              (,(format "\\(<%s *\\)%s>" date-re time-re) .
               ((lambda (tag)
                  (svg-tag-make tag :beg 1 :inverse nil :crop-right t :margin 0))))
              (,(format "<%s *\\(%s>\\)" date-re time-re) .
               ((lambda (tag)
                  (svg-tag-make tag :end -1 :inverse t :crop-left t :margin 0))))

              ;; Inactive date  (without day name, with or without time)
              (,(format "\\(\\[%s\\]\\)" date-re) .
               ((lambda (tag)
                  (svg-tag-make tag :beg 1 :end -1 :margin 0 :face 'org-date))))
              (,(format "\\(\\[%s *\\)%s\\]" date-re time-re) .
               ((lambda (tag)
                  (svg-tag-make tag :beg 1 :inverse nil :crop-right t :margin 0 :face 'org-date))))
              (,(format "\\[%s *\\(%s\\]\\)" date-re time-re) .
               ((lambda (tag)
                  (svg-tag-make tag :end -1 :inverse t :crop-left t :margin 0 :face 'org-date))))))

      (svg-tag-mode t))

    (use-package org-capture
      :ensure nil
      :custom
      (org-capture-templates
       (quote (("t" "todo" entry (file+headline "~/Notes/todo.org" "Inbox") "* TODO [#A] %?\n%a\n")))))
    (use-package org-list
      :ensure nil
      :custom
      (org-list-allow-alphabetical t)
      (org-list-demote-modify-bullet '(("+" . "-") ("-" . "+") ("*" . "+"))))
    (use-package org-src
      :ensure nil
      :custom
      (org-src-tab-acts-natively t)
      (org-src-preserve-indentation t))
    (use-package org-faces
      :ensure nil
      :custom
      (org-priority-faces '((?A . (:foreground "#F0DFAF"        :weight bold))
                            (?B . (:foreground "LightSteelBlue" :weight bold))
                            (?C . (:foreground "OliveDrab"      :weight bold))))
      (org-todo-keyword-faces
       (quote (("TODO"  :box (:line-width 1 :color nil :style none) :background "#ffaf87" :foreground "red"          :weight bold)
               ("NEXT"  :box (:line-width 1 :color nil :style none) :background "cyan"    :foreground "blue"         :weight bold)
               ("PEND"  :box (:line-width 1 :color nil :style none) :background "yellow"  :foreground "orange"       :weight bold)
               ("DONE"  :box (:line-width 1 :color nil :style none) :background "green"   :foreground "forest green" :weight bold)
               ("DEFER" :box (:line-width 1 :color nil :style none) :background "plum"    :foreground "purple"       :weight bold)
               ("SCRUB" :box (:line-width 1 :color nil :style none) :background "plum"    :foreground "purple"       :weight bold)))))

    ;; (@* "Org Agenda")
    (use-package org-agenda
      :ensure nil
      :preface
      ;; (@url :file-name "https://github.com/psamim/dotfiles/blob/master/doom/config.el#L104" :display "https://github.com/psamim/dotfiles/blob/master/doom/config.el#L104")
      (defun add-property-with-date-captured ()
        "Add DATE_CAPTURED property to the current item."
        (interactive)
        (org-set-property "CREATED" (format-time-string "%F")))
      (defun agenda-color-char ()
        (save-excursion
          (goto-char (point-min))
          (while (re-search-forward "⚡" nil t)
            (put-text-property (match-beginning 0) (match-end 0)
                               'face '(:height 300 :foreground "gold2" :bold t))))
        (save-excursion
          (goto-char (point-min))
          (while (re-search-forward org-agenda-hidden-separator nil t)
            (put-text-property (match-beginning 0) (- (match-end 0) 20)
                               'face '(:foreground "SlateGray")))))
      (defun my-org-agenda-format-date-aligned (date)
        "Format a DATE string for display in the daily/weekly agenda, or timeline.
This function makes sure that dates are aligned for easy reading."
        (use-package cal-iso :ensure nil :demand t)
        (let* ((dayname (calendar-day-name date 1 nil))
               (day (cadr date))
               (day-of-week (calendar-day-of-week date))
               (month (car date))
               (monthname (calendar-month-name month 1))
               (year (nth 2 date))
               (iso-week (org-days-to-iso-week
                          (calendar-absolute-from-gregorian date)))
               (_weekyear (cond ((and (= month 1) (>= iso-week 52))
                                 (1- year))
                                ((and (= month 12) (<= iso-week 1))
                                 (1+ year))
                                (t year)))
               (_weekstring (if (= day-of-week 1)
                                (format " W%02d" iso-week) "")))
          (format " %-2s. %2d %s, %s" dayname day monthname year)))
      :init
      ;;(add-hook 'org-agenda-finalize-hook 'org-timeline-insert-timeline :append)
      :config
      (setq org-agenda-hidden-separator nil)
      ;;(use-package org-timeline)
      :custom
      (org-agenda-start-on-weekday 0)
      (org-agenda-breadcrumbs-separator " ❱ ")
      (org-agenda-block-separator (string-to-char " "))
      (org-agenda-files (quote ("~/Desktop/notes/todo.org")))
      (org-agenda-format-date 'my-org-agenda-format-date-aligned)
      (org-agenda-category-icon-alist
       ;; (@url :file-name "https://material.io/resources/icons/?style=baseline" :display "Material icons")
       `(("work"       ,(list (all-the-icons-material "work"                  :height 1.5)) nil nil :ascent center)
         ("create"     ,(list (all-the-icons-material "create"                :height 1.5)) nil nil :ascent center)
         ("log"        ,(list (all-the-icons-material "publish"               :height 1.5)) nil nil :ascent center)
         ("invest"     ,(list (all-the-icons-material "attach_money"          :height 1.5)) nil nil :ascent center)
         ("shopping"   ,(list (all-the-icons-material "shopping_cart"         :height 1.5)) nil nil :ascent center)
         ("dining"     ,(list (all-the-icons-material "restaurant"            :height 1.5)) nil nil :ascent center)
         ("chores"     ,(list (all-the-icons-material "loop"                  :height 1.5)) nil nil :ascent center)
         ("errors"     ,(list (all-the-icons-material "bug_report"            :height 1.5)) nil nil :ascent center)
         ("problems"   ,(list (all-the-icons-material "report_problem"        :height 1.5)) nil nil :ascent center)
         ("security"   ,(list (all-the-icons-material "security"              :height 1.5)) nil nil :ascent center)
         ("videos"     ,(list (all-the-icons-material "movie"                 :height 1.5)) nil nil :ascent center)
         ("podcasts"   ,(list (all-the-icons-material "radio"                 :height 1.5)) nil nil :ascent center)
         ("legal"      ,(list (all-the-icons-material "gavel"                 :height 1.5)) nil nil :ascent center)
         ("install"    ,(list (all-the-icons-material "build"                 :height 1.5)) nil nil :ascent center)
         ("software"   ,(list (all-the-icons-material "code"                  :height 1.5)) nil nil :ascent center)
         ("reading"    ,(list (all-the-icons-material "local_library"         :height 1.5)) nil nil :ascent center)
         ("hygiene"    ,(list (all-the-icons-material "local_laundry_service" :height 1.5)) nil nil :ascent center)
         ("education"  ,(list (all-the-icons-material "school"                :height 1.5)) nil nil :ascent center)
         ("feedback"   ,(list (all-the-icons-material "feedback"              :height 1.5)) nil nil :ascent center)
         ("sharing"    ,(list (all-the-icons-material "share"                 :height 1.5)) nil nil :ascent center)
         ("phone"      ,(list (all-the-icons-material "contact_phone"         :height 1.5)) nil nil :ascent center)
         ("email"      ,(list (all-the-icons-material "contact_mail"          :height 1.5)) nil nil :ascent center)
         ("jitsi"      ,(list (all-the-icons-material "video_call"            :height 1.5)) nil nil :ascent center)
         ("exploring"  ,(list (all-the-icons-material "add_location"          :height 1.5)) nil nil :ascent center)
         ("events"     ,(list (all-the-icons-material "event"                 :height 1.5)) nil nil :ascent center)
         ("todo"       ,(list (all-the-icons-material "assignment"            :height 1.5)) nil nil :ascent center)
         ("exercise"   ,(list (all-the-icons-material "fitness_center"        :height 1.5)) nil nil :ascent center)
         ("healing"    ,(list (all-the-icons-material "healing"               :height 1.5)) nil nil :ascent center)
         ("sleep"      ,(list (all-the-icons-material "hotel"                 :height 1.5)) nil nil :ascent center)
         ("relaxing"   ,(list (all-the-icons-material "weekend"               :height 1.5)) nil nil :ascent center)))
      (org-agenda-custom-commands
       '(("o" "My Agenda" ;; (@url :file-name "https://www.reddit.com/r/emacs/comments/hnf3cw/my_orgmode_agenda_much_better_now_with_category/" :display "My org-mode agenda, much better now with category icons!")
          ((todo "TODO" ((org-agenda-overriding-header "⚡ Do Today:")
                         (org-agenda-remove-tags t)
                         (org-agenda-prefix-format (concat "  %-2i %-13b" org-agenda-hidden-separator))
                         (org-agenda-todo-keyword-format "")))
           (todo "DEFER" ((org-agenda-overriding-header "⚡ To Be Decided:")
                          (org-agenda-remove-tags t)
                          (org-agenda-prefix-format (concat "  %-2i %-13b" org-agenda-hidden-separator))
                          (org-agenda-todo-keyword-format "")))
           (todo "PEND" ((org-agenda-overriding-header "⚡ Pending Tasks:")
                         (org-agenda-remove-tags t)
                         (org-agenda-prefix-format (concat "  %-2i %-13b" org-agenda-hidden-separator))
                         (org-agenda-todo-keyword-format "")))
           (todo "NEXT" ((org-agenda-overriding-header "⚡ Jobs and Projects:")
                         (org-agenda-remove-tags t)
                         (org-agenda-prefix-format (concat "  %-2i %-13b" org-agenda-hidden-separator))
                         (org-agenda-todo-keyword-format "")))
           (agenda "" ((org-agenda-start-day "+0d")
                       (org-agenda-span 7)
                       (org-agenda-overriding-header "⚡ Schedule:")
                       (org-agenda-repeating-timestamp-show-all nil)
                       (org-agenda-remove-tags t)
                       (org-agenda-prefix-format   (concat "  %-3i  %-15b %t%s" org-agenda-hidden-separator))
                       (org-agenda-todo-keyword-format " ☐ ")
                       (org-agenda-current-time-string "┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈ Now")
                       (org-agenda-scheduled-leaders '("" ""))
                       (org-agenda-time-grid (quote ((daily today remove-match)
                                                     (0000 0300 0600 0900 1200 1500 1800 2100 2300)
                                                     "      " "┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈"))))))))))
    ;; (@* "Org Babel")
    (use-package org-babel-eval-in-repl
      :commands  org-babel-execute-src-block
      :config
      (use-package ob-core
        :ensure nil
        :custom
        (org-confirm-babel-evaluate nil))
      (defadvice org-babel-execute-src-block (around load-language nil activate)
        "Load language if needed"
        (let ((language (org-element-property :language (org-element-at-point))))
          (unless (cdr (assoc (intern language) org-babel-load-languages))
            (add-to-list 'org-babel-load-languages (cons (intern language) t))
            (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))
          ad-do-it)))

    ;; (@* "Org Mime")
    (use-package htmlize
      :ensure nil
      :commands (org-element-context org-link-set-parameters)
      :config ;; Ensure proper fontification via Org mode.
      (add-hook 'message-mode-hook
                (lambda nil
                  (local-set-key "\C-c\M-o" 'org-mime-htmlize)))
      (add-hook 'org-mode-hook
                (lambda nil
                  (local-set-key "\C-c\M-o" 'org-mime-org-buffer-htmlize)))
      (org-link-set-parameters nil :htmlize-link
                               (lambda nil
                                 (let* ((link (org-element-context))
                                        (type (org-element-property :type link))
                                        (path (org-element-property :path link)))
                                   (list :uri (format "%s:%s" type path))))))))
